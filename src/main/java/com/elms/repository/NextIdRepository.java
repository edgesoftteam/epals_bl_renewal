package com.elms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.elms.model.NextId;



@Repository("NextIdRepository")
public interface NextIdRepository extends JpaRepository<NextId, Integer> {

	@Query("FROM NextId where idName = :idName")
	NextId getNextId(@Param("idName") String idName);


	NextId findByIdName(String idName);
}
