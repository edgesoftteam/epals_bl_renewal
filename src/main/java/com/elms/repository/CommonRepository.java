package com.elms.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.elms.common.Constants;
import com.elms.exception.BasicExceptionHandler;
import com.elms.model.Activity;
import com.elms.model.EmailDetails;
import com.elms.model.EmailTemplateAdminForm;
import com.elms.model.Fee;
import com.elms.model.NextId;
import com.elms.model.Payment;
import com.elms.util.StringUtils;

@Repository
public class CommonRepository {

private static final Logger logger = LogManager.getLogger(CommonRepository.class);
	
@Autowired
JdbcTemplate  jdbcTemplate;
@Autowired
NextIdRepository nextIdRepository;
@Autowired
PlatformTransactionManager transactionManager;
@Autowired
DataSource dataSource;
@Autowired
EmailTemplateAdminForm emailTemplateAdminForm;

public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
    this.jdbcTemplate = new JdbcTemplate(dataSource);
 }
 public void setTransactionManager(PlatformTransactionManager transactionManager) {
    this.transactionManager = transactionManager;
 }

public String checkRenewalType(String renewalCode,String bussinessActNo)  throws BasicExceptionHandler{

	String sql = "SELECT ACT_NBR FROM ACTIVITY A LEFT JOIN BL_ACTIVITY BL ON BL.ACT_ID = A.ACT_ID LEFT JOIN BT_ACTIVITY BT ON BT.ACT_ID = A.ACT_ID WHERE A.ACT_ID = "+renewalCode+" "
			+ " AND (BT.BUSINESS_ACC_NO = "+bussinessActNo + " OR BL.BUSINESS_ACC_NO = " + bussinessActNo + ")";
	logger.info("ActivityInfo sql :" +sql);

	String permitNumber = "";
	try {
		permitNumber = jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
		    @Override
		    public String mapRow(ResultSet rs, int i) throws SQLException {
		    	String permitNbr;
		    	permitNbr=StringUtils.nullReplaceWithEmpty(rs.getString("ACT_NBR"));
				return permitNbr;		
		    }
		});
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		e.printStackTrace();
		logger.error("",e);
	}
	logger.debug("Before Return");
	return permitNumber;
}

/**
 * This method is to remove duplicate entries of same combination of Business Acc no & Renewal Code from 
 * TEMP_ACTIVITY_DETAILS Table. And also it insert a new record with all other details that user enters. 
 * having key- value pairs from List of Map objects
 * @param Activity Object
 * @throws BasicExceptionHandler
 * @return Activity Object 
 */
public Activity saveActivity(Activity activity){
	logger.debug("in save activity repository...");
	String deleteSql="";
	TransactionDefinition def = new DefaultTransactionDefinition();
	TransactionStatus status = transactionManager.getTransaction(def);
	    
	try{
		
		 String oldTempId = getOldTempActivityNumber(activity);
		 logger.debug("oldTemp Id  :: "+oldTempId);
		 
		 NextId nextId  = nextIdRepository.getNextId("TEMP_ID");
		 nextId.setIdValue((nextId.getIdValue()) + 1);
		 nextId.setIdName("TEMP_ID");
		 nextIdRepository.save(nextId);
		 int tempId = nextId.getIdValue();
		 activity.setTempId(StringUtils.i2s(tempId));
		 logger.debug("tempId  :"+tempId);
		 
		 deleteSql = "DELETE FROM TEMP_ACTIVITY_DETAILS WHERE RENEWAL_CODE = '" +activity.getRenewalCode().trim()+ "' AND BUSINESS_ACC_NO = '"+StringUtils.nullReplaceWithEmpty(activity.getBusinessAccNo().trim())+"'";
		 logger.debug("deleteSql  :: "+deleteSql);
		 jdbcTemplate.execute(deleteSql);
		
		logger.debug("saveActivity1"+activity.getRenewalCode());
		String insertQuery = "INSERT INTO TEMP_ACTIVITY_DETAILS (ID,RENEWAL_CODE,BUSINESS_ACC_NO) VALUES ("+tempId+",'"+activity.getRenewalCode().trim()+"', '"+activity.getBusinessAccNo().trim()+"')";
		
		logger.debug("insertQuery  :"+insertQuery);
		jdbcTemplate.update(insertQuery);
		
		if(oldTempId!=null ) {		
			String updateQuery = "UPDATE TEMP_ATTACHMENT SET TEMP_ID="+tempId+" WHERE TEMP_ID="+oldTempId ;
			logger.debug("updateQuery Temp Attachments :"+updateQuery);
			jdbcTemplate.update(updateQuery);
		}

		transactionManager.commit(status);
	}catch(Exception e){
		logger.error("",e);
		 transactionManager.rollback(status);
		 throw e;			
	}
	return activity;
}

/**
 * This method is to update the TEMP_ACTIVITY_DETAILS Table for every next button click.
 * @param activity
 * @throws BasicExceptionHandler
 * @return 
 * 
 */
public void updateUserDetails(Activity activity) throws BasicExceptionHandler{
	logger.debug("in updateUserDetails repository...");
	try{		
		String busniessName="";
		busniessName=activity.getBusinessName();
		if(busniessName != null) {
			busniessName=activity.getBusinessName().replaceAll("'", "''");	
		}

		String addr = activity.getAddress();
		if(activity.getAddress().contains("'")) {
		addr= addr.replaceAll("'", "''");
		}
		logger.debug("addr... "+addr);
		logger.debug("updateUserDetails"+activity.toString());
		String updateQuery = "UPDATE TEMP_ACTIVITY_DETAILS SET ACT_ID='"+activity.getRenewalCode()+"', ACT_NBR='"+activity.getPermitNumber()+"', EMAIL='"+activity.getEmail()+"',NAME ='"+activity.getName()+"', BUSINESS_PHONE='"+activity.getNewPhone()+"', STR_NO='"+activity.getStrNo()+"', ADDRESS='"+addr+"', UNIT='"+activity.getUnit()+"', CITY='"+activity.getCity()+"', STATE='"+activity.getState()+"', ZIP='"+activity.getZip()+"', BUSINESS_NAME='"+busniessName+"' WHERE ID="+activity.getTempId() ;
		logger.debug("updateQuery  :"+updateQuery);
		jdbcTemplate.update(updateQuery);
	
	}catch(Exception e){
		logger.error("",e);
		throw new BasicExceptionHandler(e);		
	}
}

/**
 * This method is to fetch the LKUP_SYSTEM Table in the form of single Map object 
 * having key- value pairs.
 * @param lkupSystemDataList
 * @throws Exception
 * @return Map<String,String>
 * 
 */
public Map<String, String> getLkupSystemDataMap(){        
    String sql = "SELECT * FROM LKUP_SYSTEM";
    logger.info(sql);
    Map<String, String> lkupSystemData = null;
    try {
    	lkupSystemData= jdbcTemplate.query(sql, new ResultSetExtractor<Map<String, String>>() {                
	        @Override
	        public Map<String, String> extractData(ResultSet rs) throws SQLException, DataAccessException {    
	            Map<String, String> info = new HashMap<String, String>();
	            while(rs.next()) {
	                info.put(rs.getString("NAME"),rs.getString("VALUE"));                    
	            }            
	            return info;
	        }
    	});
    }catch(Exception e) {
    	logger.error("Error occured in getLkupSystemDataMap :: " +e.getMessage());
    	e.printStackTrace();
    }
    return lkupSystemData;       
}

/**
 * This method is to check if the Fee for BL/BT is payed or the Renewal application 
 * is submitted based on the renewal code or activity Id.
 * @param String
 * @throws Exception
 * @return String
 * 
 */
public String getBlOrBtActivityRenewalOnlineFlag(Activity activity) {
	String sql = "SELECT A.RENEWAL_ONLINE FROM ACTIVITY A LEFT JOIN BL_ACTIVITY BL ON BL.ACT_ID = A.ACT_ID LEFT JOIN BT_ACTIVITY BT ON BT.ACT_ID = A.ACT_ID WHERE A.ACT_ID = "+activity.getRenewalCode();
	
	if(activity.getPermitNumber()!=null && activity.getPermitNumber().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BL)) {
		sql = sql + " AND BL.BUSINESS_ACC_NO = " +activity.getBusinessAccNo();
	}else {
		sql = sql + " AND BT.BUSINESS_ACC_NO = " +activity.getBusinessAccNo();
	}
			    
	logger.debug("getActivityRenewalOnline sql :"+sql);
	String renewalOnline = "N";
	try {
		renewalOnline=jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
			
		    @Override
		    public String mapRow(ResultSet rs, int i) throws SQLException {
				String renewalOnline=rs.getString("RENEWAL_ONLINE");
				return renewalOnline;	
		    }
		});
	} catch(EmptyResultDataAccessException e1) {
		logger.error("",e1);
	}catch (Exception e) {
		logger.error("",e);
	}	
	return renewalOnline;
}

/**
 * This method is to fetch Fee list for BL/BT after payment of the Renewal application 
 * based on the renewal code or activity Id.
 * @param String, String
 * @throws Exception
 * @return List<Fee>
 * 
 */

public List<Fee> getFeesFromTempActFees(String actId,String tempId) {
	String sql ="SELECT DISTINCT F.FEE_ID,TAF.FEE_UNITS,TAF.FEE_AMNT,TAF.TOTAL_FEE_AMNT,F.FEE_DESC FROM TEMP_ACTIVITY_FEE TAF JOIN FEE F ON TAF.FEE_ID=F.FEE_ID WHERE TAF.ACTIVITY_ID = "+actId+" AND TAF.TEMP_ID = "+tempId;
	
	logger.info("getFees sql :"+sql);
	List<Fee> feeList = new ArrayList<Fee>(); 
	try {
		feeList = jdbcTemplate.query(sql,new RowMapper<Fee>() {
        @Override
        public Fee mapRow(ResultSet rs, int i) throws SQLException {
        	Fee fee = new Fee();
        	fee.setFeeId(rs.getString("FEE_ID"));
        	fee.setFeeUnits(StringUtils.s2d(rs.getString("FEE_UNITS")));
        	fee.setFeeAmnt(StringUtils.s2d(rs.getString("FEE_AMNT")));
        	fee.setTotalFee(StringUtils.s2d(rs.getString("TOTAL_FEE_AMNT")));
        	fee.setFeeDesc(rs.getString("FEE_DESC"));        	
		return  fee;
        }
    });
	
     logger.debug("# of elements set in List - " + feeList.size());
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
	}
	return feeList;
}

/**
 * This method is to fetch Fee Account for each Fee id that is passed as variable to this method after payment of the Renewal application 
 * based on the fee Id.
 * @param String, String
 * @throws Exception
 * @return List<Fee>
 * 
 */
public String getFeesAccountForFeeId(String feeId) {
	String sql ="SELECT FEE_ACCOUNT FROM FEE WHERE FEE_ID = "+feeId;
	
	logger.debug("getFees sql :"+sql);
	String feeAccount = "";
	try {
		feeAccount = jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
        @Override
        public String mapRow(ResultSet rs, int i) throws SQLException {
        	String feeAccount;
        	feeAccount = StringUtils.nullReplaceWithEmpty(rs.getString("FEE_ACCOUNT"));        	
		return  feeAccount;
        }
    });
	
     logger.debug("# feeAccount - " + feeAccount);
	} catch(EmptyResultDataAccessException e1) {
		e1.printStackTrace();
	} catch (Exception e) {
		logger.error("",e);
}
	return feeAccount;
}

/**
 * This method is to check if the payment is fully done or partially done for BL/BT Renewal application 
 * is submitted based on the renewal code or activity Id.
 * @param String
 * @throws Exception
 * @return String 
 */
public double getFullOrPartialPaymentFlag(Activity activity) {
	String sql = "select (SUM(FEE_AMNT)-SUM(FEE_PAID)) AS AVERAGE from activity_fee WHERE ACTIVITY_ID = "+activity.getRenewalCode();
				    
	logger.debug("getFullOrPartialPaymentFlag sql :"+sql);
	double partialPay=0.0;
	String partialPayFlag = null;
	try {
		partialPayFlag=jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
			
		    @Override
		    public String mapRow(ResultSet rs, int i) throws SQLException {
				String partialPayFlag=rs.getString("AVERAGE");
				return partialPayFlag;	
		    }
		});
		logger.debug("partialPayFlag... "+partialPayFlag);
	} catch(EmptyResultDataAccessException e1) {
		logger.error("",e1);
	}catch (Exception e) {
		logger.error("",e);
	}	
	partialPay=StringUtils.s2d(partialPayFlag);
	return partialPay;
}

	/**
	 * This method is to fetch the online flag based on Activity Type for validating login
	 * @param Activity Form
	 * @throws 
	 * @return String 
	 */
	public String getActTypeOnlineFlag(Activity activityForm) {
		String sql ="SELECT B.ONLINE_YN FROM ACTIVITY A, LKUP_ACT_TYPE B WHERE A.ACT_NBR = '"+activityForm.getPermitNumber()+"' AND A.ACT_TYPE = B.TYPE";
		
		logger.debug("getActTypeOnlineFlag sql :"+sql);
		String actTypeOnlineFlag = "";
		try {
			actTypeOnlineFlag = jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
	        @Override
	        public String mapRow(ResultSet rs, int i) throws SQLException {
	        	String flag;
	        	flag = StringUtils.nullReplaceWithEmpty(rs.getString("ONLINE_YN"));        	
			return  flag;
	        }
	    });
		
	     logger.debug("# ActTypeOnlineFlag - " + actTypeOnlineFlag);
		} catch(EmptyResultDataAccessException e1) {
			e1.printStackTrace();
		} catch (Exception e) {
			logger.error("",e);
	}
		return actTypeOnlineFlag;
	}
	
	/**
	 * This method is to fetch the Email Templates based on email type
	 * @param String
	 * @throws 
	 * @return EmailTemplateAdmin Form 
	 */
	public EmailTemplateAdminForm getEmailTempByTempTypeId(String tempType) {
		logger.info("getEmailTempByTempTypeId("+tempType+")");
		StringBuilder sql = new StringBuilder();
		EmailTemplateAdminForm emailTemplateAdminForm = new EmailTemplateAdminForm();
		try {
			sql.append("SELECT A.EMAIL_TEMP_TYPE_ID, B.EMAIL_TEMPLATE_ID, B.EMAIL_SUBJECT, B.EMAIL_BODY"
	    			+ " FROM LKUP_EMAIL_TEMPLATE_TYPE A, EMAIL_TEMPLATE B, REF_EMAIL_TEMPLATE C WHERE A.EMAIL_TEMP_TYPE_ID = C.EMAIL_TEMP_TYPE_ID"
	    			+ " AND B.EMAIL_TEMPLATE_ID = C.EMAIL_TEMPLATE_ID ");
	    	if(tempType!=null && !tempType.equalsIgnoreCase("")) {
	    		sql.append(" AND A.EMAIL_TEMP_TYPE = '" +tempType +"'");        		
	    	}
	    	sql.append(" ORDER BY A.EMAIL_TEMP_TYPE");
	    	logger.debug("Sql is :: " + sql.toString());       	
	
	    	emailTemplateAdminForm = jdbcTemplate.queryForObject(sql.toString(),new RowMapper<EmailTemplateAdminForm>() {
	            @Override
	            public EmailTemplateAdminForm mapRow(ResultSet rs, int i) throws SQLException {
	            	EmailTemplateAdminForm emailTempForm = new EmailTemplateAdminForm();
	            	emailTempForm.setEmailTempTypeId(rs.getInt("EMAIL_TEMP_TYPE_ID"));
	            	emailTempForm.setEmailTempId(rs.getInt("EMAIL_TEMPLATE_ID"));
	            	emailTempForm.setEmailSubject((rs.getString("EMAIL_SUBJECT") != null ? rs.getString("EMAIL_SUBJECT") : "").trim());
	            	emailTempForm.setEmailMessage((rs.getString("EMAIL_BODY") != null ? rs.getString("EMAIL_BODY") : "").trim());  
					return emailTempForm;
	            }
			});
			return emailTemplateAdminForm;
		} catch (Exception e) {
			logger.error("Unable to get email templates by id "+e);			
		}
		return emailTemplateAdminForm;
	}
	public void resetBlUploadAttachments(String tempId) {
		String updateQuery = "delete from TEMP_ATTACHMENT where TEMP_ID = "+tempId;
		logger.debug("updateQuery  :"+updateQuery);
		jdbcTemplate.update(updateQuery);		
	}

	/**
	 * This method is to fetch the Email Templates based on email type
	 * @param emailDetails
	 * @throws 
	 * @return EmailTemplateAdmin Form 
	 */
	public EmailTemplateAdminForm getEmailData(EmailDetails emailDetails) {		
		try {		    
			String emailType = emailDetails.getEmailType();
			
			if(emailDetails.getPermitNumber()!=null && emailDetails.getPermitNumber().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BT)){			
				if(emailType!=null && emailType.equalsIgnoreCase(Constants.RENEWAL_APPLICATION_SUBMIT_EMAIL_TO_APPLICANT_BT)){
					logger.debug("in APPLICATION_SUBMIT_EMAIL_APPLICANT_BT");				
					emailTemplateAdminForm = getEmailTempByTempTypeId(Constants.RENEWAL_APPLICATION_SUBMIT_EMAIL_TO_APPLICANT_BT);
					emailTemplateAdminForm.setEmailMessage(getEmailBodyWithValues(emailTemplateAdminForm.getEmailMessage(), emailDetails));			    
				}
				logger.debug("BT email subject :: " +emailTemplateAdminForm.getEmailSubject());
				logger.debug("BT email Body Message :: " +emailTemplateAdminForm.getEmailMessage());
			}
			
			if(emailDetails.getPermitNumber()!=null && emailDetails.getPermitNumber().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BL)){			
				if(emailType!=null && emailType.equalsIgnoreCase(Constants.RENEWAL_APPLICATION_SUBMIT_EMAIL_TO_APPLICANT_BL)){
					logger.debug("in APPLICATION_SUBMIT_EMAIL_APPLICANT_BL");				
					emailTemplateAdminForm = getEmailTempByTempTypeId(Constants.RENEWAL_APPLICATION_SUBMIT_EMAIL_TO_APPLICANT_BL);
					emailTemplateAdminForm.setEmailMessage(getEmailBodyWithValues(emailTemplateAdminForm.getEmailMessage(), emailDetails));			    
				}
				logger.debug("BL email subject :: " +emailTemplateAdminForm.getEmailSubject());
				logger.debug("BL email Body Message :: " +emailTemplateAdminForm.getEmailMessage());
			}
			logger.error("Email message sent successfully");
			return emailTemplateAdminForm;
		} catch (EmptyResultDataAccessException e1) {
			logger.error("Empty Result Data Access Exception e1 :: "	+ e1.getMessage()+e1);
			e1.printStackTrace();
			return new EmailTemplateAdminForm();
		}catch (Exception e) {
			logger.error("Error in sending emails "	+ e.getMessage()+e);
			e.printStackTrace();
			return new EmailTemplateAdminForm();
		}
	}
	
	/**
	 * This method is to replace all the original values which has ## in the beginning & ending of the string.
	 * This will return complete email body language with values.
	 * @param String emailBody
	 * @param EmailDetails emailDetails
	 * @throws 
	 * @return String
	 * 
	 */
	private String getEmailBodyWithValues(String emailBody, EmailDetails emailDetails) {
		logger.error("emailBody :"+emailBody);
		logger.error("emailDetails.getBusinessName() :"+emailDetails.getBusinessName());
		logger.error("emailDetails.getNoOfEmp() :"+emailDetails.getNoOfEmployees());	
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
				LocalDate currentDate = LocalDate.now();
				logger.debug(dtf.format(currentDate));
				
		if(emailBody!=null) {
			if(emailDetails.getPermitNumber()!=null && emailDetails.getPermitNumber().startsWith("BL")) {
				if(emailDetails.getNoOfEmployees()==null && emailDetails.getNoOfEmployees().equalsIgnoreCase("0") && emailDetails.getNoOfEmployees().equalsIgnoreCase("")) emailDetails.setNoOfEmployees("1");
			}
			if(emailDetails.getNoOfEmployees()!=null && !emailDetails.getNoOfEmployees().trim().equalsIgnoreCase("")) {
				if(emailBody.contains("##TOTAL_COUNT##")) emailBody = emailBody.replaceAll("##TOTAL_COUNT##", emailDetails.getNoOfEmployees());
				if(emailBody.contains("##NUMBER_OF_PAYMENTS##")) emailBody = emailBody.replace("##NUMBER_OF_PAYMENTS##", emailDetails.getNoOfEmployees());
				logger.error("emailBody after total count :"+emailDetails.getNoOfEmployees());
			}
			if(currentDate != null) {
				logger.debug("currentDate :"+currentDate);
				if(emailBody.contains("##PAYMENT_DATE##")) emailBody = emailBody.replaceAll("##PAYMENT_DATE##", currentDate.toString());
				if(emailBody.contains("##SUBMIT_DATE##")) emailBody = emailBody.replaceAll("##SUBMIT_DATE##", currentDate.toString());
				if(emailBody.contains("##APPLICATION_DATE##")) emailBody = emailBody.replaceAll("##APPLICATION_DATE##", currentDate.toString());
				logger.error("currentDate :"+currentDate);
			}
			if(StringUtils.s2d(emailDetails.getTotalFee()) > 0) {
				if(emailBody!=null && emailBody.contains("##PAYMENT_AMOUNT##")) emailBody = emailBody.replace("##PAYMENT_AMOUNT##", ("$".concat(emailDetails.getTotalFee())));
				if(emailBody!=null && emailBody.contains("##TOTAL_SUM_OF_PAYMENTS##")) emailBody = emailBody.replace("##TOTAL_SUM_OF_PAYMENTS##", ("$".concat(emailDetails.getTotalFee())));
				logger.error("##PAYMENT_AMOUNT## :: " +StringUtils.s2d(emailDetails.getTotalFee()));			
				logger.error("##TOTAL_SUM_OF_PAYMENTS## :: " +StringUtils.s2d(emailDetails.getTotalFee()));
			}
			if(emailDetails.getBusinessName() != null && !emailDetails.getBusinessName().trim().equalsIgnoreCase("")) {
				if(emailBody.contains("##BUSINESS_NAME##")) emailBody = emailBody.replace("##BUSINESS_NAME##", emailDetails.getBusinessName());
				logger.error("##BUSINESS_NAME## :: " +emailDetails.getBusinessName());
			}
			if(StringUtils.s2d(emailDetails.getPaymentAmount()) > 0) {
				if(emailBody!=null && emailBody.contains("##PAYMENT_AMOUNT##")) emailBody = emailBody.replace("##PAYMENT_AMOUNT##", ("$".concat(emailDetails.getPaymentAmount())));
				logger.error("##PAYMENT_AMOUNT## :: " +emailDetails.getPaymentAmount());			
			}
			if(currentDate != null) {
				if(emailBody!=null && emailBody.contains("##APPLICATION_DATE##")) emailBody = emailBody.replace("##APPLICATION_DATE##", currentDate.toString());
				if(emailBody!=null && emailBody.contains("##APPLICATIONDATE##")) emailBody = emailBody.replace("##APPLICATIONDATE##", currentDate.toString());
				logger.error("##APPLICATION_DATE## :: " +currentDate.toString());
			}
		}
		emailBody= emailBody.replace("##", "");
		emailBody= emailBody.replace("\\\"", "\"");
		logger.error("emailBody before method exit: "+emailBody);
		return emailBody;
	}
	
	/**
	 * This method is to fetch old temp id for updating the documents in TEMP_ATTACHMENT table.
	 * This is to not to re-upload the documents which they have done earlier.
	 * @param String Business Account number
	 * @param String Renewal Code
	 * @throws Exception
	 * @return String
	 * 
	 */
	public String getOldTempActivityNumber(Activity activity) {
		String selectSql = "SELECT ID FROM TEMP_ACTIVITY_DETAILS WHERE RENEWAL_CODE = '" +activity.getRenewalCode()+ "' AND BUSINESS_ACC_NO = '"+StringUtils.nullReplaceWithEmpty(activity.getBusinessAccNo())+"'";
		 logger.error("selectSql getOldTempActivityNumber() :: "+selectSql);
		
		 String oldTempId = "0";
		try {
			oldTempId=jdbcTemplate.queryForObject(selectSql,new RowMapper<String>() {				
			    @Override
			    public String mapRow(ResultSet rs, int i) throws SQLException {
			    	String oldTempId = null;
			    		logger.error("*****************");
			    		oldTempId=rs.getString("ID");
					return oldTempId;	
			    }
			});
		} catch(EmptyResultDataAccessException e1) {
			//logger.error("",e1);
		}catch (Exception e) {
			//logger.error("",e);
		}	
		logger.error("Old Temp Id for updating the Temp Attchment Tabel :: " +oldTempId);
		return oldTempId;
	}
	public JSONObject savePaymentLog(Map<String, String> datamap) {
		
		String actId = datamap.get("actId"); 
		String amount =  datamap.get("amount");
		if(amount != null) {
			amount = amount.replaceAll(",", "");
		}
		String X_FP_SEQUENCE = datamap.get("x_fp_sequence");
		String comboNo = datamap.get("comboNo");
		String levelId = datamap.get("levelId");
		String comboName = datamap.get("comboName");
		JSONObject obj = new JSONObject();
		try {
			NextId nextId  = nextIdRepository.getNextId("PAYMENT_LOG_ID");
			 nextId.setIdValue((nextId.getIdValue()) + 1);
			 nextId.setIdName("PAYMENT_LOG_ID");
			 nextIdRepository.save(nextId);
			 int paymentLogId = nextId.getIdValue();
			String sql = "INSERT INTO PAYMENT_LOG_INFO (ID, ACT_ID, AMOUNT, X_FP_SEQUENCE, X_FP_TIMESTAMP, CREATED,COMBO_NO,LEVEL_ID,COMBO_NAME) VALUES ("+paymentLogId+", "+actId+", "+amount+", '"+X_FP_SEQUENCE+"', current_timestamp, current_timestamp ,'"+comboNo+"','"+levelId+"','"+comboName+"')";
			logger.debug("sql : "+sql);
			jdbcTemplate.update(sql);
			
			obj.put("status", "data saved successfully");
		} catch(EmptyResultDataAccessException e) {		
			logger.error("",e);
		}catch (Exception e1) {
			logger.error("",e1);
		}	
		return obj;
	}
	public String getKeyValue(String keyName) { 
		String sql = "SELECT VALUE FROM LKUP_SYSTEM WHERE NAME=" + StringUtils.checkString(keyName);
		logger.debug("config sql :"+sql);
		String value="";
		try {
			value=jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
			    @Override
			    public String mapRow(ResultSet rs, int i) throws SQLException {
					
			    	String value =rs.getString("VALUE");	
			    	return value;	
			    }
			});
		} catch(EmptyResultDataAccessException e1) {
			e1.printStackTrace();
		} catch (Exception e) {
			logger.error("",e);
		}	
		return value;
	}
	public double getPaidAmount(Activity activity) {
		String sql = "select SUM(FEE_PAID) AS FEE_PAID from activity_fee WHERE ACTIVITY_ID = "+activity.getRenewalCode();
					    
		logger.info("getFullOrPartialPaymentFlag sql :"+sql);
		double amount=0.0;
		String payFlag = null;
		try {
			payFlag=jdbcTemplate.queryForObject(sql,new RowMapper<String>() {
				
			    @Override
			    public String mapRow(ResultSet rs, int i) throws SQLException {
					String paidAmount=rs.getString("FEE_PAID");
					return paidAmount;	
			    }
			});
			logger.debug("getPaidAmount... "+payFlag);
		} catch(EmptyResultDataAccessException e1) {
			logger.error("",e1);
		}catch (Exception e) {
			logger.error("",e);
		}	
		amount=StringUtils.s2d(payFlag);
		return amount;
	}
	
	public void savePaymentLog(Payment payment) {
		
		try {
			NextId nextId  = nextIdRepository.getNextId("PAYMENT_LOG_ID");
			 nextId.setIdValue((nextId.getIdValue()) + 1);
			 nextId.setIdName("PAYMENT_LOG_ID");
			 nextIdRepository.save(nextId);
			 int paymentLogId = nextId.getIdValue();
			 String sql = "INSERT INTO PAYMENT_LOG_INFO (ID, ACT_ID, AMOUNT, X_FP_SEQUENCE, X_FP_TIMESTAMP, CREATED,COMBO_NO,LEVEL_ID,COMBO_NAME,COMBO) VALUES ("+paymentLogId+", "+payment.getActivityId()+", "+payment.getAmount()+", null, current_timestamp, current_timestamp ,'"+payment.getActivityNumber()+"','"+payment.getActivityId()+"','Online Renewal Portal',"+StringUtils.checkString("BLBTRenewal")+")";
			 logger.debug("sql : "+sql);
			 jdbcTemplate.update(sql);
		}catch (Exception e) {
			  logger.error("error occured while savePaymentLog : "+e.getMessage());
		}
	}
	
}