package com.elms.common;

public class Version {

	public static String number = "4.7.14";

	public static String date = "07/11/2024";  


	/**
	 * @return
	 */
	public static String getDate() {
		return date;
	}

	/**
	 * @return
	 */
	public static String getNumber() {
		return number;
	}

	/**
	 * @param string
	 */
	public void setDate(String string) {
		date = string;
	}

	/**
	 * @param string
	 */
	public void setNumber(String string) {
		number = string;
	}

}
