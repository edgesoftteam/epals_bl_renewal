/*
 * Created on May 08, 2019
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.elms.util;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * @author Gayathri Turlapati
 *
 */
public class EmailAuthenticator extends Authenticator {

    protected String username;
    protected String password;

    public EmailAuthenticator(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(username, password);
    }
}
