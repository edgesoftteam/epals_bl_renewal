package com.elms.model;

import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class EmailDetails {	
	
	private String emailType;
	private String emailId;
	private String businessName;
	private String noOfEmployees;
	private String permitNumber;
	private String totalFee;
	private String paymentAmount;
	private Map<String, String> lkupSystemDataMap;
	EmailTemplateAdminForm emailTemplateAdminForm;
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EmailDetails [emailType=" + emailType + ", emailId=" + emailId + ", businessName=" + businessName
				+ ", noOfEmployees=" + noOfEmployees + ", permitNumber=" + permitNumber + ", totalFee=" + totalFee
				+ "]";
	}
	
	/**
	 * @return the emailType
	 */
	public String getEmailType() {
		return emailType;
	}
	/**
	 * @param emailType the emailType to set
	 */
	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}
	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}
	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	/**
	 * @return the businessName
	 */
	public String getBusinessName() {
		return businessName;
	}
	/**
	 * @param businessName the businessName to set
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	/**
	 * @return the noOfEmployees
	 */
	public String getNoOfEmployees() {
		return noOfEmployees;
	}
	/**
	 * @param noOfEmployees the noOfEmployees to set
	 */
	public void setNoOfEmployees(String noOfEmployees) {
		this.noOfEmployees = noOfEmployees;
	}
	/**
	 * @return the permitNumber
	 */
	public String getPermitNumber() {
		return permitNumber;
	}
	/**
	 * @param permitNumber the permitNumber to set
	 */
	public void setPermitNumber(String permitNumber) {
		this.permitNumber = permitNumber;
	}
	/**
	 * @return the totalFee
	 */
	public String getTotalFee() {
		return totalFee;
	}
	/**
	 * @param totalFee the totalFee to set
	 */
	public void setTotalFee(String totalFee) {
		this.totalFee = totalFee;
	}

	/**
	 * @return the paymentAmount
	 */
	public String getPaymentAmount() {
		return paymentAmount;
	}

	/**
	 * @param paymentAmount the paymentAmount to set
	 */
	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	/**
	 * @return the lkupSystemDataMap
	 */
	public Map<String, String> getLkupSystemDataMap() {
		return lkupSystemDataMap;
	}

	/**
	 * @param lkupSystemDataMap the lkupSystemDataMap to set
	 */
	public void setLkupSystemDataMap(Map<String, String> lkupSystemDataMap) {
		this.lkupSystemDataMap = lkupSystemDataMap;
	}

	/**
	 * @return the emailTemplateAdminForm
	 */
	public EmailTemplateAdminForm getEmailTemplateAdminForm() {
		return emailTemplateAdminForm;
	}

	/**
	 * @param emailTemplateAdminForm the emailTemplateAdminForm to set
	 */
	public void setEmailTemplateAdminForm(EmailTemplateAdminForm emailTemplateAdminForm) {
		this.emailTemplateAdminForm = emailTemplateAdminForm;
	}	
}