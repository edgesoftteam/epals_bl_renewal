package com.elms.model;

import org.springframework.stereotype.Component;

@Component
public class BmcDescription {
	
	private String bmcName;
	private String bmcLink;
	/**
	 * @return the bmcName
	 */
	public String getBmcName() {
		return bmcName;
	}
	/**
	 * @param bmcName the bmcName to set
	 */
	public void setBmcName(String bmcName) {
		this.bmcName = bmcName;
	}
	/**
	 * @return the bmcLink
	 */
	public String getBmcLink() {
		return bmcLink;
	}
	/**
	 * @param bmcLink the bmcLink to set
	 */
	public void setBmcLink(String bmcLink) {
		this.bmcLink = bmcLink;
	}
	
	
	
}
