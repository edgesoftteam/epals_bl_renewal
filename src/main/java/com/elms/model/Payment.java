/**
 * 
 */
package com.elms.model;

import java.io.Serializable;

/**
 * @author arjun
 *
 */
public class Payment implements Serializable {
	
	/**
	 * System generate serial UID
	 */
	private static final long serialVersionUID = 3555109083041770462L;
	
	
	protected String activityId;
	protected String invoiceId;
	protected int paymentId;
	protected int peopleId;
	protected String activityNumber;
	protected String email;
	protected String activityType;
	protected String firstName;
	protected String lastName;
	protected String address;
	protected String city;
	protected String state;
	protected String zip;
	protected String phoneNumber;
	protected String cardNumber;
	protected String expMonth;
	protected String expYear;
	protected String cvv;
	protected String termsCondition;
	protected String amount;
	protected String transactionId;
	protected String userName;
	protected String userId;
	protected String authCode;
	protected String paymentMethod;
	protected String transactionStatus;
	protected String transactionResponseCode;
	protected String transactionResponse;
	protected String ipAddress;
	public Payment() {
		super();
	}
	public Payment(String activityId, String invoiceId, String activityNumber, String email, String activityType,
			String firstName, String lastName, String address, String city, String state, String zip,
			String phoneNumber, String cardNumber, String expMonth, String expYear, String cvv, String termsCondition,
			String amount, String transactionId, String userName, String userId, String authCode, String paymentMethod,
			String transactionStatus, String transactionResponseCode, String transactionResponse, String ipAddress) {
		super();
		this.activityId = activityId;
		this.invoiceId = invoiceId;
		this.activityNumber = activityNumber;
		this.email = email;
		this.activityType = activityType;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.phoneNumber = phoneNumber;
		this.cardNumber = cardNumber;
		this.expMonth = expMonth;
		this.expYear = expYear;
		this.cvv = cvv;
		this.termsCondition = termsCondition;
		this.amount = amount;
		this.transactionId = transactionId;
		this.userName = userName;
		this.userId = userId;
		this.authCode = authCode;
		this.paymentMethod = paymentMethod;
		this.transactionStatus = transactionStatus;
		this.transactionResponseCode = transactionResponseCode;
		this.transactionResponse = transactionResponse;
		this.ipAddress = ipAddress;
	}
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public String getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}
	public String getActivityNumber() {
		return activityNumber;
	}
	public void setActivityNumber(String activityNumber) {
		this.activityNumber = activityNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getActivityType() {
		return activityType;
	}
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getExpMonth() {
		return expMonth;
	}
	public void setExpMonth(String expMonth) {
		this.expMonth = expMonth;
	}
	public String getExpYear() {
		return expYear;
	}
	public void setExpYear(String expYear) {
		this.expYear = expYear;
	}
	public String getCvv() {
		return cvv;
	}
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	public String getTermsCondition() {
		return termsCondition;
	}
	public void setTermsCondition(String termsCondition) {
		this.termsCondition = termsCondition;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getAuthCode() {
		return authCode;
	}
	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getTransactionStatus() {
		return transactionStatus;
	}
	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	public String getTransactionResponseCode() {
		return transactionResponseCode;
	}
	public void setTransactionResponseCode(String transactionResponseCode) {
		this.transactionResponseCode = transactionResponseCode;
	}
	public String getTransactionResponse() {
		return transactionResponse;
	}
	public void setTransactionResponse(String transactionResponse) {
		this.transactionResponse = transactionResponse;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	@Override
	public String toString() {
		return "Payment [activityId=" + activityId + ", invoiceId=" + invoiceId + ", activityNumber=" + activityNumber
				+ ", email=" + email + ", activityType=" + activityType + ", firstName=" + firstName + ", lastName="
				+ lastName + ", address=" + address + ", city=" + city + ", state=" + state + ", zip=" + zip
				+ ", phoneNumber=" + phoneNumber + ", cardNumber=" + cardNumber + ", expMonth=" + expMonth
				+ ", expYear=" + expYear + ", cvv=" + cvv + ", termsCondition=" + termsCondition + ", amount=" + amount
				+ ", transactionId=" + transactionId + ", userName=" + userName + ", userId=" + userId + ", authCode="
				+ authCode + ", paymentMethod=" + paymentMethod + ", transactionStatus=" + transactionStatus
				+ ", transactionResponseCode=" + transactionResponseCode + ", transactionResponse="
				+ transactionResponse + ", ipAddress=" + ipAddress + "]";
	}
	public int getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}
	public int getPeopleId() {
		return peopleId;
	}
	public void setPeopleId(int peopleId) {
		this.peopleId = peopleId;
	}
	
	
}
