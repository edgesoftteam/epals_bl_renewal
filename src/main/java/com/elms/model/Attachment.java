/**
 * 
 */
package com.elms.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author Gayathri Turlapati
 *
 */
public class Attachment implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3280207766202799693L;
	
	private int attachmentId;
	private int attachmentTypeId;
	private String attachmentDesc;
	private String onlineAvailable;
	private String fileName;
	private String fileLoc;
	private String fileSize;
	private String documentURL;
	private String comments;
	private String downloadOrUploadType;
	private String bmcDescription;
	private String bmcPopUpWindowUrl;
	private Map<String, String> bmcLinkDescMap;
	private List<String> commentsList;
	/**
	 * @return the attachmentId
	 */
	public int getAttachmentId() {
		return attachmentId;
	}
	/**
	 * @param attachmentId the attachmentId to set
	 */
	public void setAttachmentId(int attachmentId) {
		this.attachmentId = attachmentId;
	}
	/**
	 * @return the attachmentTypeId
	 */
	public int getAttachmentTypeId() {
		return attachmentTypeId;
	}
	/**
	 * @param attachmentTypeId the attachmentTypeId to set
	 */
	public void setAttachmentTypeId(int attachmentTypeId) {
		this.attachmentTypeId = attachmentTypeId;
	}
	/**
	 * @return the attachmentDesc
	 */
	public String getAttachmentDesc() {
		return attachmentDesc;
	}
	/**
	 * @param attachmentDesc the attachmentDesc to set
	 */
	public void setAttachmentDesc(String attachmentDesc) {
		this.attachmentDesc = attachmentDesc;
	}
	/**
	 * @return the onlineAvailable
	 */
	public String getOnlineAvailable() {
		return onlineAvailable;
	}
	/**
	 * @param onlineAvailable the onlineAvailable to set
	 */
	public void setOnlineAvailable(String onlineAvailable) {
		this.onlineAvailable = onlineAvailable;
	}
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @return the fileLoc
	 */
	public String getFileLoc() {
		return fileLoc;
	}
	/**
	 * @param fileLoc the fileLoc to set
	 */
	public void setFileLoc(String fileLoc) {
		this.fileLoc = fileLoc;
	}
	/**
	 * @return the fileSize
	 */
	public String getFileSize() {
		return fileSize;
	}
	/**
	 * @param fileSize the fileSize to set
	 */
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	/**
	 * @return the documentURL
	 */
	public String getDocumentURL() {
		return documentURL;
	}
	/**
	 * @param documentURL the documentURL to set
	 */
	public void setDocumentURL(String documentURL) {
		this.documentURL = documentURL;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the downloadOrUploadType
	 */
	public String getDownloadOrUploadType() {
		return downloadOrUploadType;
	}
	/**
	 * @param downloadOrUploadType the downloadOrUploadType to set
	 */
	public void setDownloadOrUploadType(String downloadOrUploadType) {
		this.downloadOrUploadType = downloadOrUploadType;
	}
	/**
	 * @return the bmcDescription
	 */
	public String getBmcDescription() {
		return bmcDescription;
	}
	/**
	 * @param bmcDescription the bmcDescription to set
	 */
	public void setBmcDescription(String bmcDescription) {
		this.bmcDescription = bmcDescription;
	}
	/**
	 * @return the bmcPopUpWindowUrl
	 */
	public String getBmcPopUpWindowUrl() {
		return bmcPopUpWindowUrl;
	}
	/**
	 * @param bmcPopUpWindowUrl the bmcPopUpWindowUrl to set
	 */
	public void setBmcPopUpWindowUrl(String bmcPopUpWindowUrl) {
		this.bmcPopUpWindowUrl = bmcPopUpWindowUrl;
	}
	/**
	 * @return the bmcLinkDescMap
	 */
	public Map<String, String> getBmcLinkDescMap() {
		return bmcLinkDescMap;
	}
	/**
	 * @param bmcLinkDescMap the bmcLinkDescMap to set
	 */
	public void setBmcLinkDescMap(Map<String, String> bmcLinkDescMap) {
		this.bmcLinkDescMap = bmcLinkDescMap;
	}
	/**
	 * @return the commentsList
	 */
	public List<String> getCommentsList() {
		return commentsList;
	}
	/**
	 * @param commentsList the commentsList to set
	 */
	public void setCommentsList(List<String> commentsList) {
		this.commentsList = commentsList;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Attachment [attachmentId=" + attachmentId + ", attachmentTypeId=" + attachmentTypeId
				+ ", attachmentDesc=" + attachmentDesc + ", onlineAvailable=" + onlineAvailable + ", fileName="
				+ fileName + ", fileLoc=" + fileLoc + ", fileSize=" + fileSize + ", documentURL=" + documentURL
				+ ", comments=" + comments + ", downloadOrUploadType=" + downloadOrUploadType + ", bmcDescription="
				+ bmcDescription + ", bmcPopUpWindowUrl=" + bmcPopUpWindowUrl + ", bmcLinkDescMap=" + bmcLinkDescMap
				+ ", commentsList=" + commentsList + "]";
	}	
}