package com.elms.serviceImp;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.DatatypeFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;

import com.elms.common.Constants;
import com.elms.exception.BasicExceptionHandler;
import com.elms.model.Activity;
import com.elms.model.Attachment;
import com.elms.model.EmailDetails;
import com.elms.model.Fee;
import com.elms.model.Payment;
import com.elms.repository.BusinessLicenseRenewalRepository;
import com.elms.repository.CommonRepository;
import com.elms.service.BusinessLicenseRenewalService;
import com.elms.service.CommonService;
import com.elms.util.EmailSender;
import com.elms.util.StringUtils;

import net.authorize.Environment;
import net.authorize.Merchant;
import net.authorize.TransactionType;
import net.authorize.aim.Result;
import net.authorize.aim.Transaction;
import net.authorize.api.contract.v1.ArrayOfBatchDetailsType;
import net.authorize.api.contract.v1.BatchDetailsType;
import net.authorize.api.contract.v1.CreateTransactionRequest;
import net.authorize.api.contract.v1.CreateTransactionResponse;
import net.authorize.api.contract.v1.CreditCardType;
import net.authorize.api.contract.v1.CustomerAddressType;
import net.authorize.api.contract.v1.GetSettledBatchListRequest;
import net.authorize.api.contract.v1.GetSettledBatchListResponse;
import net.authorize.api.contract.v1.GetTransactionListRequest;
import net.authorize.api.contract.v1.GetTransactionListResponse;
import net.authorize.api.contract.v1.MerchantAuthenticationType;
import net.authorize.api.contract.v1.MessageTypeEnum;
import net.authorize.api.contract.v1.OrderType;
import net.authorize.api.contract.v1.Paging;
import net.authorize.api.contract.v1.PaymentType;
import net.authorize.api.contract.v1.TransactionListOrderFieldEnum;
import net.authorize.api.contract.v1.TransactionListSorting;
import net.authorize.api.contract.v1.TransactionRequestType;
import net.authorize.api.contract.v1.TransactionResponse;
import net.authorize.api.contract.v1.TransactionTypeEnum;
import net.authorize.api.controller.CreateTransactionController;
import net.authorize.api.controller.GetSettledBatchListController;
import net.authorize.api.controller.GetTransactionListController;
import net.authorize.api.controller.base.ApiOperationBase;
import net.authorize.data.Customer;
import net.authorize.data.Order;
import net.authorize.data.creditcard.CreditCard;


@Service
public class BusinessLicenseRenewalServiceImpl implements BusinessLicenseRenewalService {

	private static final Logger logger = LogManager.getLogger(BusinessLicenseRenewalServiceImpl.class);

	@Autowired
	BusinessLicenseRenewalRepository  businessLicenseRenewalRepository;
	
	@Autowired
	CommonRepository  commonRepository;
	
	@Autowired
	CommonService commonService;
	
	@Autowired
	EmailDetails emailDetails;
	
	/*
	 * This method is to save the Business Account Number and 
	 * Renewal Code (activity Id) in the temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	@Override
	public Activity saveBLRenewalDetails(Activity blActivityForm ) throws BasicExceptionHandler {
		logger.debug("inside save of activityDetails in service"+blActivityForm.getTempId());
		Activity activityDetails = new Activity();
		
		logger.debug("blActivityForm...   "+blActivityForm.toString());
		
		activityDetails = businessLicenseRenewalRepository.getBLActivityInfo(blActivityForm.getBusinessAccNo(),blActivityForm.getRenewalCode());
		logger.debug("activityDetails...   "+activityDetails.toString());
		if(StringUtils.s2i(activityDetails.getRenewalCode()) > 0) {
			commonService.saveActivity(blActivityForm);			
		}
		
		activityDetails = businessLicenseRenewalRepository.getBLActivityInfo(blActivityForm.getBusinessAccNo(),blActivityForm.getRenewalCode());
		activityDetails.setTempId(activityDetails.getTempId());
		return activityDetails;
	
	}


	/*
	 * This method is to update user details to temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	@Override
	public Activity saveUserDetails(Activity activity) throws BasicExceptionHandler {

		Activity activityDetails = new Activity();
		commonService.updateUserDetails(activity);	
		activityDetails=businessLicenseRenewalRepository.getBLActivityInfo(activity.getBusinessAccNo(),activity.getRenewalCode());
		
		
		if(activityDetails.getLinePattern().equalsIgnoreCase("1") && (activityDetails.getNoOfEmp() == null || activityDetails.getNoOfEmp() == "")) {
			activityDetails.setNoOfEmp(activityDetails.getQtyOther());
		}
        
		List<Attachment> attachmentList = businessLicenseRenewalRepository.getAttachmentList(StringUtils.s2i(activity.getRenewalCode()),activity.getTempId());
    	
		Activity act=businessLicenseRenewalRepository.getQtyFlag(activity.getRenewalCode());
		activityDetails.setBlQtyFlag(act.getBlQtyFlag());
		
		activityDetails.setAttachmentList(attachmentList);
        logger.debug("attachmentList List size:: " +attachmentList.size());
        
		return activityDetails;
	}


	/*
	 * This method is to save BL quantity details to temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	@Override
	public Activity saveQtyDetails(Activity activity) {

		Activity activityDetails = new Activity();
		activityDetails=businessLicenseRenewalRepository.updateEmp(activity);

		activityDetails=businessLicenseRenewalRepository.getBLActivityInfo(activity.getBusinessAccNo(),activity.getRenewalCode());
		List<Attachment> attachmentList = businessLicenseRenewalRepository.getAttachmentList(StringUtils.s2i(activity.getRenewalCode()),activity.getTempId());
		activityDetails.setAttachmentList(attachmentList);
		logger.debug("attachmentList List size:: " +attachmentList.size());
				
		return activityDetails;
	}


	/*
	 * This method is to save BL attachments details to temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	@Override
	public Activity saveAttachmentDetails(Activity activity,String attachmentTypeId) {
		logger.debug("business acc no = "+activity.getBusinessAccNo());
		logger.debug("renewal code = "+activity.getRenewalCode());
		Activity activityDetails=businessLicenseRenewalRepository.getBLActivityInfo(activity.getBusinessAccNo(),activity.getRenewalCode());
		logger.debug("inside attachUploads 286...");

        // obtains the upload file part in this multipart request
        List<MultipartFile> filesList=null;
        long size = 0;
		try {
			filesList=activity.getTheFile();
			logger.debug("size.... "+filesList.size());
			List<String> fileNames = new ArrayList<String>();
			if(null != filesList && filesList.size() > 0) {
				for (MultipartFile multipartFile : filesList) {
					if(multipartFile != null && multipartFile.getOriginalFilename() !=null) {
						String fileName = multipartFile.getOriginalFilename();
						size=multipartFile.getSize();
						fileNames.add(fileName);	
					}
				}
			}

			logger.debug("size... "+size);
			if(size/1024/1024 > 50) {
				activityDetails.setDisplayErrorMsg("You have exceeded the maximum allowed size for an attachment");
			}else {
				activityDetails.setDisplaySuccessMsg("You have successfully uploaded the file");
				
			}
	        if (filesList != null && size/1024/1024 <=50) {
		        businessLicenseRenewalRepository.saveStreamToFile(activity,attachmentTypeId);
	        }
	        List<Attachment> attachmentList = businessLicenseRenewalRepository.getAttachmentList(StringUtils.s2i(activity.getRenewalCode()),activity.getTempId());
			activity.setAttachmentList(attachmentList);
			logger.debug("attachmentList List size:: " +attachmentList.size());

			activityDetails.setAttachmentList(attachmentList);
		} catch(MultipartException e1) {
			e1.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}

		return activityDetails;
	}


	/*
	 * This method is to save BL fee details to temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	@Override
	public Activity saveFeeDetails(Activity activity) throws BasicExceptionHandler {

		List<Fee> feeList=businessLicenseRenewalRepository.getFees(activity.getActType(),activity.getTempId());
		activity.setFeeList(feeList);
		//logger.debug("activity... "+activity.toString());
		Activity activityDetails=new Activity();

		logger.debug("inside feeDetails "+activity.toString());
		activityDetails=businessLicenseRenewalRepository.getBLActivityInfo(activity.getBusinessAccNo(),activity.getRenewalCode());
		
		Map<String, String> lkupSystemData =   commonService.getLkupSystemDataMap();
        String lateFeeFromDate = "";
        String lateFeeToDate = "";
        logger.debug("lkupSystemData.size() :: " +lkupSystemData.size());
       
            if(lkupSystemData.get(Constants.BL_LATE_FEE_FROM_DATE)!=null) {
                lateFeeFromDate = lkupSystemData.get(Constants.BL_LATE_FEE_FROM_DATE);
            }                         
            if(lkupSystemData.get(Constants.BL_LATE_FEE_TO_DATE)!=null) {
                lateFeeToDate = lkupSystemData.get(Constants.BL_LATE_FEE_TO_DATE);
            }
		
		double totalFee = 0;
		double lateFeeFactor = 0;
		double licenseFeeAnnualTotalFactor = 0;

		SimpleDateFormat formatter = new SimpleDateFormat("ddMMMyyyy");  
		 String strDate = formatter.format(new Date());  
         
		for (int i=0;i<feeList.size();i++) {
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.LATE_FEE)) {
				logger.debug("getDayOfMonth() :: " +StringUtils.getDayOfMonth() + ":: getMonth :: " +StringUtils.getMonth());

				   if (StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeFromDate)) >= 0 && StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeToDate)) <= 0) {
					lateFeeFactor = feeList.get(i).getFeeFactor();
					logger.debug("lateFeeFactor ::  " +lateFeeFactor);				
				}
			}
			
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().startsWith(Constants.LICENSE_FEE_ANNUAL)) {
				licenseFeeAnnualTotalFactor = feeList.get(i).getFeeTotal();
				logger.debug("licenseFeeAnnualTotalFactor ::  " +licenseFeeAnnualTotalFactor);
			}
		}
		
		for (int i=0;i<feeList.size();i++) {
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.LATE_FEE)) {
				logger.debug("getDayOfMonth() :: " +StringUtils.getDayOfMonth() + ":: getMonth :: " +StringUtils.getMonth());

				   if (StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeFromDate)) >= 0 && StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeToDate)) <= 0) {
					totalFee = totalFee + (lateFeeFactor * licenseFeeAnnualTotalFactor);// When it is Late Fee, get Factor instead of feeFactor	 
					feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(lateFeeFactor * licenseFeeAnnualTotalFactor)));
					feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(lateFeeFactor * licenseFeeAnnualTotalFactor));
					logger.debug("totalFee... " +totalFee);					
				}else {
					feeList.get(i).setFeeTotal(StringUtils.s2d(""));
					feeList.get(i).setFeeTotalStr("");
					feeList.get(i).setFeeDesc("");
				}
			}
			logger.debug("feeList.get(i).getFeeDesc() .... " +feeList.get(i).getFeeDesc() + " :: Total Fee :: " +totalFee);
			
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BL_ADMINISTRATION_FEE_DESC)) {
				logger.debug("feeList.get(i).getFeeDesc() .... " +feeList.get(i).getFeeDesc() + " :: Total Fee :: " +totalFee);
				feeList.get(i).setFeeTotal(StringUtils.s2d(""));
				feeList.get(i).setFeeTotalStr("");
				feeList.get(i).setFeeDesc("");
			}
			if(feeList.get(i).getFeeDesc()!=null && !feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BL_ADMINISTRATION_FEE_DESC) && !feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.LATE_FEE)) {
				totalFee = totalFee + feeList.get(i).getFeeTotal();// When it is other than Late Fee, get FeeFactor instead of Factor					
				logger.debug("Line 324  Fee details.... " +feeList.get(i).getFeeTotal() + " :: Total Fee :: " +totalFee);
			}
		}	
		activityDetails.setTotalFee(StringUtils.roundOffDouble(totalFee));
		activity.setTotalFee(StringUtils.roundOffDouble(totalFee));

		activityDetails.setFeeList(feeList);
		return activityDetails;
	}


	/*
	 * This method is to get preview page details from temp table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	@Override
	public Activity getPreviewPageDetails(Activity activity) throws BasicExceptionHandler {

		Activity activityDetails=businessLicenseRenewalRepository.getTempActivityDetails(activity.getTempId());
        logger.debug("activity.getTotalFee() : " +activityDetails.toString());
       
		activityDetails=businessLicenseRenewalRepository.getBLActivityInfo(activity.getBusinessAccNo(),activity.getRenewalCode());
		List<Fee> feeList=businessLicenseRenewalRepository.getFees(activity.getActType(),activity.getTempId());

		Map<String, String> lkupSystemData =   commonService.getLkupSystemDataMap();
        String lateFeeFromDate = "";
        String lateFeeToDate = "";
        logger.debug("lkupSystemData.size() :: " +lkupSystemData.size());
       
            if(lkupSystemData.get(Constants.BL_LATE_FEE_FROM_DATE)!=null) {
                lateFeeFromDate = lkupSystemData.get(Constants.BL_LATE_FEE_FROM_DATE);
            }                         
            if(lkupSystemData.get(Constants.BL_LATE_FEE_TO_DATE)!=null) {
                lateFeeToDate = lkupSystemData.get(Constants.BL_LATE_FEE_TO_DATE);
            }
		
		double totalFee = 0;
		double feeFactor = 0;
		
		double lateFeeFactor = 0;
		double administrationFactor = 0;
		double licenseFeeAnnualTotalFactor = 0;
		double disabilityFeeTotalFactor = 0;
		double lateFeeTotalFactor = 0;

		SimpleDateFormat formatter = new SimpleDateFormat("ddMMMyyyy");
		 String strDate = formatter.format(new Date());  
		     
		for (int i=0;i<feeList.size();i++) {
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BL_ADMINISTRATION_FEE_DESC)) {
				administrationFactor = feeList.get(i).getFactor();
				logger.debug("administrationFactor ::  " +administrationFactor);	
			}
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.LATE_FEE)) {
				logger.debug("getDayOfMonth() :: " +StringUtils.getDayOfMonth() + ":: getMonth :: " +StringUtils.getMonth());
				if (StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeFromDate)) >= 0 && StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeToDate)) <= 0) {
				   lateFeeFactor = feeList.get(i).getFeeFactor();				   						
				   logger.debug("lateFeeFactor ::  " +lateFeeFactor);				
				}
			}			
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().startsWith(Constants.LICENSE_FEE_ANNUAL)) {
				licenseFeeAnnualTotalFactor = feeList.get(i).getFeeTotal();
				logger.debug("licenseFeeAnnualTotalFactor ::  " +licenseFeeAnnualTotalFactor);				
			}
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().startsWith(Constants.CA_STATE_DISABILITY_FEE)) {
				 disabilityFeeTotalFactor = feeList.get(i).getFeeTotal();
					logger.debug("licenseFeeAnnualTotalFactor ::  " +licenseFeeAnnualTotalFactor);
			}			
		}
		
		if(lateFeeFactor>0 && licenseFeeAnnualTotalFactor>0) {
			lateFeeTotalFactor = (lateFeeFactor * licenseFeeAnnualTotalFactor);
			logger.debug("lateFeeTotalFactor ::  " +lateFeeTotalFactor);
		}
		
		for (int i=0;i<feeList.size();i++) {						
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.CA_STATE_DISABILITY_FEE)) {
				totalFee = totalFee + disabilityFeeTotalFactor;// When it is other than Late Fee & Administration Fee, add the totals directly
				feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeList.get(i).getFeeTotal()));//Assigning the Fee total to Fee total Str for showing it in the UI for maintaining the digits after the period (XX.XX pattern)
			}
			
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().startsWith(Constants.LICENSE_FEE_ANNUAL)) {
				totalFee = totalFee + licenseFeeAnnualTotalFactor;// When it is other than Late Fee & Administration Fee, add the totals directly
				feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeList.get(i).getFeeTotal()));//Assigning the Fee total to Fee total Str for showing it in the UI for maintaining the digits after the period (XX.XX pattern)
			}
			
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.LATE_FEE)) {
				logger.debug("getDayOfMonth() :: " +StringUtils.getDayOfMonth() + ":: getMonth :: " +StringUtils.getMonth());

				if (StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeFromDate)) >= 0 && StringUtils.toDateFormatDDMMMYYYY(strDate).compareTo(StringUtils.toDateFormatDDMMMYYYY(lateFeeToDate)) <= 0) {
					totalFee = totalFee + lateFeeTotalFactor;// When it is Late Fee, get Factor instead of feeFactor	
					feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(lateFeeTotalFactor)));
					feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(lateFeeTotalFactor));
				}else {
					feeList.get(i).setFeeTotal(StringUtils.s2d(""));
					feeList.get(i).setFeeTotalStr("");
					feeList.get(i).setFeeDesc("");
				}
			}
			
			if(feeList.get(i).getFeeDesc()!=null && feeList.get(i).getFeeDesc().equalsIgnoreCase(Constants.BL_ADMINISTRATION_FEE_DESC)) {
				feeFactor = (licenseFeeAnnualTotalFactor + disabilityFeeTotalFactor + lateFeeTotalFactor)*administrationFactor;				
				totalFee = totalFee + feeFactor;
				feeList.get(i).setFeeTotal(StringUtils.s2d(StringUtils.roundOffDouble(feeFactor))); // To show the calculated value as fee factor on preview screen
				feeList.get(i).setFeeTotalStr(StringUtils.roundOffDouble(feeFactor));
			}
			logger.debug("FeeDesc :: " +feeList.get(i).getFeeDesc() + ":: FeeFactor :: " +feeList.get(i).getFeeFactor() + " :: Total Fee :: " +totalFee);
			activityDetails.setQtyOther(feeList.get(i).getNoOfQuantity());
		}				
		activityDetails.setTotalFee(StringUtils.roundOffDouble(totalFee));
		activityDetails.setFeeList(feeList);
		businessLicenseRenewalRepository.saveBLFeeDetails(activityDetails);
		return activityDetails;
	}

	/*
	 * This method is to save BL attachments details to attachments table.
	 * 
	 * @return Activity Object
	 * @Parameters Activity Object
	 */
	@Override
	public Activity saveReUploadAttachments(Activity activity,String attachmentTypeId) {
		
		logger.debug("inside attachUploads 286..."+activity.getRenewalCode());

    // obtains the upload file part in this multipart request
    List<MultipartFile> filesList=null;
    long size = 0;
	try {
		filesList=activity.getTheFile();
		logger.debug("size.... "+filesList.size());
		List<String> fileNames = new ArrayList<String>();
		if(null != filesList && filesList.size() > 0) {
			for (MultipartFile multipartFile : filesList) {
				if(multipartFile != null && multipartFile.getOriginalFilename() !=null) {
					String fileName = multipartFile.getOriginalFilename();
					size=multipartFile.getSize();
					fileNames.add(fileName);	
				}
			}
		}

		if(size/1024/1024 > 50) {
			activity.setDisplayErrorMsg("You have exceeded the maximum allowed size for an attachment");
		}else {
			activity.setDisplaySuccessMsg("You have successfully uploaded the file");
		}
        if (filesList != null && size/1024/1024 <=50) {
	        businessLicenseRenewalRepository.saveStreamToAttachment(activity,attachmentTypeId);
        }
        List<Attachment> attachmentList = businessLicenseRenewalRepository.getAttachmentListForReUpload(activity.getRenewalCode());
		activity.setAttachmentList(attachmentList);
		logger.debug("attachmentList List size:: " +attachmentList.size());

	} catch(MultipartException e1) {
		e1.printStackTrace();
	}catch (Exception e) {
		e.printStackTrace();
	}

		return activity;
	}


	/**
	 * This method is for checking whether the particular business type file is uploaded or not
	 * 
	 * @throws IOException
	 * @return response object, renewalCode
	 * @Parameters
	 */
	@Override
	public void checkFileUploadedOrNot(HttpServletResponse response, int renewalCode) throws IOException {
		JSONObject jsonObj = businessLicenseRenewalRepository.checkFileUploadedOrNot(renewalCode, "0");	
		response.setStatus(200);
		response.setContentType("application/json");
		response.getWriter().write(jsonObj.toString());
	}

	/**
	 * To get the attachment list after reset upload details in BL
	 * @param renewalCode
	 * @param tempId
	 * @return
	 */
	@Override
	public List<Attachment> getAttachmentList(int renewalCode, String tempId) {
		List<Attachment> attachmentList = businessLicenseRenewalRepository.getAttachmentList(renewalCode,tempId);
		return attachmentList;
	}

	@Override
	public Activity payment(Activity activity){
		Payment payment = new Payment();
		String merchant="";
		try{
		 String prodFlag = commonRepository.getKeyValue(Constants.PROD_SERVER_FLAG);
		 String environment = commonRepository.getKeyValue("PAYMENT_AUTHORIZE_ENVIRONMENT");
		 
		 String apiLogin ="";
		 String transactionkey ="";			 
		 MerchantAuthenticationType merchantAuthenticationType  = new MerchantAuthenticationType();
		 if(environment!=null && environment.equalsIgnoreCase("PRODUCTION")) {
			 ApiOperationBase.setEnvironment(Environment.PRODUCTION);
		 }else {
			 ApiOperationBase.setEnvironment(Environment.SANDBOX); // SANDBOX  , 463d5BEjDB, 7XPtd28e38eFE3pn
		 }
		if(prodFlag.equalsIgnoreCase("Y")){	
			logger.debug("environment : "+environment);
			apiLogin =commonRepository.getKeyValue("PAYMENT_AUTHORIZE_BLBT_ECOM_API_LOGIN");
		    transactionkey =commonRepository.getKeyValue("PAYMENT_AUTHORIZE_BLBT_ECOM_TRANSACTIONKEY");
		    merchant="PAYMENT_AUTHORIZE_BLBT_ECOM_API_LOGIN";
		}else{
			apiLogin =commonRepository.getKeyValue("PAYMENT_TEST_API_LOGIN");
		    transactionkey =commonRepository.getKeyValue("PAYMENT_TEST_TRANSACTIONKEY");
		    merchant="PAYMENT_TEST_API_LOGIN";
		}
			if(apiLogin!=null & !apiLogin.equalsIgnoreCase("") && transactionkey!=null && !transactionkey.equalsIgnoreCase("")){
				 merchantAuthenticationType.setName(apiLogin);
			     merchantAuthenticationType.setTransactionKey(transactionkey);
			 }
			
			ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);
			
		    payment.setActivityId(activity.getRenewalCode());
		    payment.setActivityNumber(activity.getPermitNumber());
		    payment.setActivityType(activity.getActType());
		    payment.setAmount(activity.getTotalFee());
		    payment.setAddress(activity.getAddress());
		    payment.setCardNumber(activity.getCardNumber());
		    payment.setExpYear(activity.getExpYear());
		    payment.setExpMonth(activity.getExpMonth());
		    payment.setCvv(activity.getCvv());
		    payment.setFirstName(activity.getCardHolderName());
		    payment.setPaymentMethod("online");
		    payment.setLastName(activity.getCardHolderLastName());
		    payment.setAddress(activity.getPayeeAddress());
		    payment.setCity(activity.getPayeeCity());
		    payment.setState(activity.getPayeeState());
		    payment.setZip(activity.getPayeeZip());
		    //payment.setInvoiceId(businessLicenseRenewalRepository.getInvoiceId(payment.getActivityId()));
		    
		    commonRepository.savePaymentLog(payment);
		    
		    PaymentType paymentType = new PaymentType();
	        CreditCardType creditCard = new CreditCardType();
	        creditCard.setCardNumber(payment.getCardNumber());
	        creditCard.setExpirationDate(payment.getExpMonth()+""+payment.getExpYear());
	        paymentType.setCreditCard(creditCard);
	       
	        // Create the payment transaction request
	        TransactionRequestType txnRequest = new TransactionRequestType();
	        txnRequest.setTransactionType(TransactionTypeEnum.AUTH_CAPTURE_TRANSACTION.value());
	        txnRequest.setPayment(paymentType);
	        txnRequest.setAmount(new BigDecimal(payment.getAmount()).setScale(2, RoundingMode.CEILING));
	        CustomerAddressType cAddress = new CustomerAddressType();
			cAddress.setFirstName(payment.getFirstName());
			cAddress.setLastName(payment.getLastName());
		
			cAddress.setAddress(payment.getAddress());
			cAddress.setCity(payment.getCity());
			cAddress.setState(payment.getState());
			cAddress.setZip(payment.getZip());
			txnRequest.setBillTo(cAddress);
			OrderType order = new OrderType();
			
			txnRequest.setRefTransId(payment.getActivityId());	 
			order.setInvoiceNumber(payment.getActivityId());
			order.setDescription(payment.getActivityNumber());
			txnRequest.setOrder(order);
			
	        // Make the API Request
	        CreateTransactionRequest apiRequest = new CreateTransactionRequest();
	        apiRequest.setTransactionRequest(txnRequest);
	        apiRequest.setRefId(payment.getActivityId());
	        
	        CreateTransactionController controller = new CreateTransactionController(apiRequest);
	        controller.execute();
	        
	        CreateTransactionResponse response = controller.getApiResponse();
	        
	        if (response!=null) {
	        	// If API Response is ok, go ahead and check the transaction response
	        	 logger.debug("Response Code : "+ response.getTransactionResponse().getResponseCode());
	  	        logger.debug("Transaction Id: " + response.getTransactionResponse().getTransId());
	  	        logger.debug("AuthorizationCode : "+response.getTransactionResponse().getAuthCode());
	  	        logger.debug("Ref Id : "+response.getRefId());
	  	      payment.setTransactionId(response.getTransactionResponse().getTransId());
	  	      payment.setTransactionResponseCode(response.getTransactionResponse().getResponseCode());
	  	      payment.setAuthCode(response.getTransactionResponse().getAuthCode());
		  	    activity.setAuthCode(payment.getAuthCode());
		        activity.setOnlineTransactionId(payment.getTransactionId());
		        activity.setResponseText(payment.getTransactionResponse());
		        
	        	if (response.getMessages().getResultCode() == MessageTypeEnum.OK) {
	        		TransactionResponse result = response.getTransactionResponse();
	        		
	        		if (result.getMessages() != null) {
	        			if(response.getTransactionResponse().getResponseCode()!=null && response.getTransactionResponse().getResponseCode().equalsIgnoreCase("1")) {
	                    payment.setTransactionResponse(response.getTransactionResponse().getMessages().getMessage().get(0).getDescription());
	                    payment.setTransactionStatus("Approved");  
	                    activity=businessLicenseRenewalRepository.saveApplicationDetails(activity,payment,merchant);
	    	        	EmailSender emailSender = new EmailSender();
	    	        	Map<String, String> lkupSystemDataMap = new HashMap<String,String>(); 
	    	        	lkupSystemDataMap = commonService.getLkupSystemDataMap();
	    	        	try {
	    	    			
	    	    			emailDetails.setBusinessName(activity.getBusinessName());
	    	    			emailDetails.setEmailId(activity.getEmail());    			
	    	    			emailDetails.setNoOfEmployees(activity.getNoOfEmp());
	    	    			emailDetails.setPermitNumber(activity.getPermitNumber());
	    	    			emailDetails.setTotalFee(activity.getTotalFee());
	    	    			emailDetails.setLkupSystemDataMap(lkupSystemDataMap);
	    	    			if(emailDetails.getPermitNumber()!=null && emailDetails.getPermitNumber().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BT)){
	    		    			emailDetails.setEmailType(Constants.RENEWAL_APPLICATION_SUBMIT_EMAIL_TO_APPLICANT_BT);
	    		    			emailDetails.setEmailTemplateAdminForm(commonService.getEmailData(emailDetails));
	    		        		emailSender.sendEmail(emailDetails);
	    	    			}else {
	    	    				emailDetails.setEmailType(Constants.RENEWAL_APPLICATION_SUBMIT_EMAIL_TO_APPLICANT_BL);
	    	    				emailDetails.setEmailTemplateAdminForm(commonService.getEmailData(emailDetails));
	    	    				emailSender.sendEmail(emailDetails);
	    	    			}
	    	    		}catch (Exception e) { 
	    	    			logger.error(""+e);
	    	    			e.printStackTrace();
	    				}
	    	            businessLicenseRenewalRepository.deleteTempActivity(activity.getTempId());
	    	            activity.setResponseCode("1");

	    	            }else {
	        			logger.debug("Failed Transaction.");
	        			if (response.getTransactionResponse().getErrors() != null) {
	        				logger.debug("Error Code: " + response.getTransactionResponse().getErrors().getError().get(0).getErrorCode());
	        				logger.debug("Error message: " + response.getTransactionResponse().getErrors().getError().get(0).getErrorText());
	        				payment.setTransactionResponse(response.getTransactionResponse().getErrors().getError().get(0).getErrorText());
	                        payment.setTransactionResponseCode(response.getMessages().getResultCode().toString());
	            			payment.setTransactionStatus("Error");
	            			businessLicenseRenewalRepository.saveOnlinePaymentTransaction(payment,merchant);
	        			}else{
	        			payment.setTransactionResponse(response.getTransactionResponse().getMessages().getMessage().get(0).getDescription());
	        			payment.setTransactionStatus("Error");
	        			businessLicenseRenewalRepository.saveOnlinePaymentTransaction(payment,merchant);
	        			}
	        		}
	        		}else {
	        			logger.debug("Failed Transaction.");
	        			if (response.getTransactionResponse().getErrors() != null) {
	        				logger.debug("Error Code: " + response.getTransactionResponse().getErrors().getError().get(0).getErrorCode());
	        				logger.debug("Error message: " + response.getTransactionResponse().getErrors().getError().get(0).getErrorText());
	        				payment.setTransactionResponse(response.getTransactionResponse().getErrors().getError().get(0).getErrorText());
	                        payment.setTransactionResponseCode(response.getMessages().getResultCode().toString());
	            			payment.setTransactionStatus("Error");
	            			businessLicenseRenewalRepository.saveOnlinePaymentTransaction(payment,merchant);
	        			}
	        		}
	        	}else {
	        		logger.debug("Failed Transaction.");
	        		if (response.getTransactionResponse() != null && response.getTransactionResponse().getErrors() != null) {
	        			logger.debug("Error Code: " + response.getTransactionResponse().getErrors().getError().get(0).getErrorCode());
	        			logger.debug("Error message: " + response.getTransactionResponse().getErrors().getError().get(0).getErrorText());
	        			payment.setTransactionResponse(response.getTransactionResponse().getErrors().getError().get(0).getErrorText());
	                    payment.setTransactionResponseCode(response.getTransactionResponse().getErrors().getError().get(0).getErrorCode());
	        			payment.setTransactionStatus("Error");
	        			businessLicenseRenewalRepository.saveOnlinePaymentTransaction(payment,merchant);
	        		}
	        		else {
	        			logger.debug("Error Code: " + response.getMessages().getMessage().get(0).getCode());
	        			logger.debug("Error message: " + response.getMessages().getMessage().get(0).getText());
	        			payment.setTransactionResponse(response.getMessages().getMessage().get(0).getText());
	                    payment.setTransactionResponseCode(response.getMessages().getMessage().get(0).getCode());
	        			payment.setTransactionStatus("Error");
	        			businessLicenseRenewalRepository.saveOnlinePaymentTransaction(payment,merchant);
	        		}
	        	}
	        }else {
	        	logger.debug("Null Response.");
	        	payment.setTransactionStatus("Error");
	        	payment.setTransactionResponse("Tansaction failed while sending payment");
	        	businessLicenseRenewalRepository.saveOnlinePaymentTransaction(payment,merchant);
	        }
	        
		}catch (Exception e) {
			payment.setTransactionStatus("Error");
			payment.setTransactionResponse("Error occred while sending payment");
			businessLicenseRenewalRepository.saveOnlinePaymentTransaction(payment,merchant);
			logger.error("Error occred while sending payment : "+e.getMessage());
			e.printStackTrace();
		}
		
		activity.setAuthCode(payment.getAuthCode());
		activity.setOnlineTransactionId(payment.getTransactionId());
		activity.setResponseText(payment.getTransactionResponse());
		
			return activity;
	    }
	
	@Override
	public void getTransactionList(String firstSettlementDate, String lastSettlementDate) {

		try{
		 String prodFlag = commonRepository.getKeyValue(Constants.PROD_SERVER_FLAG);
		 String environment = commonRepository.getKeyValue("PAYMENT_AUTHORIZE_ENVIRONMENT");
		 
		 String apiLogin ="";
		 String transactionkey ="";			 
		 MerchantAuthenticationType merchantAuthenticationType  = new MerchantAuthenticationType();
		 if(environment!=null && environment.equalsIgnoreCase("PRODUCTION")) {
			 ApiOperationBase.setEnvironment(Environment.PRODUCTION);
		 }else {
			 ApiOperationBase.setEnvironment(Environment.SANDBOX); // SANDBOX  , 463d5BEjDB, 7XPtd28e38eFE3pn
		 }
		if(prodFlag.equalsIgnoreCase("Y")){	
			logger.debug("environment : "+environment);
			apiLogin =commonRepository.getKeyValue("PAYMENT_AUTHORIZE_BLBT_ECOM_API_LOGIN");
		    transactionkey =commonRepository.getKeyValue("PAYMENT_AUTHORIZE_BLBT_ECOM_TRANSACTIONKEY");
		}else{
			apiLogin =commonRepository.getKeyValue("PAYMENT_TEST_API_LOGIN");
		    transactionkey =commonRepository.getKeyValue("PAYMENT_TEST_TRANSACTIONKEY");
		}
			if(apiLogin!=null & !apiLogin.equalsIgnoreCase("") && transactionkey!=null && !transactionkey.equalsIgnoreCase("")){
				 merchantAuthenticationType.setName(apiLogin);
			     merchantAuthenticationType.setTransactionKey(transactionkey);
			 }
			
			ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);
			
			 GetSettledBatchListRequest getRequest = new GetSettledBatchListRequest();
		     getRequest.setMerchantAuthentication(merchantAuthenticationType);
			
		     try {
		            // Set first settlement date in format (year, month, day)(should not be less that 31 days since last settlement date)
		            GregorianCalendar pastDate = new GregorianCalendar();
		            pastDate.add(Calendar.DAY_OF_YEAR, -7);
		            logger.debug("pastDate : "+pastDate);
		            getRequest.setFirstSettlementDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(firstSettlementDate));

		            // Set last settlement date in format (year, month, day) (should not be greater that 31 days since first settlement date)
		            GregorianCalendar currentDate = new GregorianCalendar();
		            logger.debug("currentDate : "+currentDate);
		            getRequest.setLastSettlementDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(lastSettlementDate));

		        } catch (Exception ex) {
		        	logger.error("Error : while setting dates");
		            ex.printStackTrace();
		        }
		     
		        GetSettledBatchListController controller = new GetSettledBatchListController(getRequest);
		        controller.execute();
		        GetSettledBatchListResponse getResponse = controller.getApiResponse();
		        if (getResponse != null) {

		            if (getResponse.getMessages().getResultCode() == MessageTypeEnum.OK) {

		            	logger.debug(getResponse.getMessages().getMessage().get(0).getCode());
		            	logger.debug(getResponse.getMessages().getMessage().get(0).getText());

		                ArrayOfBatchDetailsType batchList = getResponse.getBatchList();
		                if (batchList != null) {
		                	logger.debug("List of Settled Transaction :");
		                    for (BatchDetailsType batch : batchList.getBatch()) {
		                        logger.debug(batch.getBatchId() + " - " + batch.getMarketType() + " - " + batch.getPaymentMethod() + " - " + batch.getProduct() + " - " + batch.getSettlementState()+" - "+ batch.getSettlementTimeUTC());
		                        getTransactionListByBatchId(batch.getBatchId() , apiLogin, transactionkey);
		                    }
		                }
		            } else {
		            	logger.debug("Failed to get settled batch list:  " + getResponse.getMessages().getResultCode());
		            	logger.debug(getResponse.getMessages().getMessage().get(0).getText());
		            }
		        }
		     
		     
		}catch (Exception e) {
			logger.error("Error occred while sending payment : "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void getTransactionListByBatchId(String batchId,String apiLoginId,String transactionKey) {
		try {

			ApiOperationBase.setEnvironment(Environment.SANDBOX);

	        MerchantAuthenticationType merchantAuthenticationType  = new MerchantAuthenticationType() ;
	        merchantAuthenticationType.setName(apiLoginId);
	        merchantAuthenticationType.setTransactionKey(transactionKey);
	        ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

			//String batchId = "4594221";
	
			GetTransactionListRequest getRequest = new GetTransactionListRequest();
			getRequest.setMerchantAuthentication(merchantAuthenticationType);
			getRequest.setBatchId(batchId);
			
	        Paging paging = new Paging();
	        paging.setLimit(100);
	        paging.setOffset(1);
	        
			getRequest.setPaging(paging);
			
			TransactionListSorting sorting = new TransactionListSorting();
			sorting.setOrderBy(TransactionListOrderFieldEnum.ID);
			sorting.setOrderDescending(true);
			
			getRequest.setSorting(sorting);

			GetTransactionListController controller = new GetTransactionListController(getRequest);
            controller.execute();

			GetTransactionListResponse getResponse = controller.getApiResponse();
			if (getResponse!=null) {
	            System.out.println("ResultCode:  " + getResponse.getMessages().getResultCode());

			     if (getResponse.getMessages().getResultCode() == MessageTypeEnum.OK) {
			        System.out.println(getResponse.getMessages().getMessage().get(0).getCode());
			        System.out.println(getResponse.getMessages().getMessage().get(0).getText());
			        logger.debug("Transaction Size : "+getResponse.getTransactions().getTransaction().size());
			        
			        for(int i=0; i<getResponse.getTransactions().getTransaction().size(); i++) {
				        logger.debug("Transaction id: "+getResponse.getTransactions().getTransaction().get(i).getTransId());
				        logger.debug("Transaction SubmitTimeUTC: "+getResponse.getTransactions().getTransaction().get(i).getSubmitTimeUTC());
				        logger.debug("Transaction SettleAmount: "+getResponse.getTransactions().getTransaction().get(i).getSettleAmount());
				        logger.debug("Transaction Transaction Status: "+getResponse.getTransactions().getTransaction().get(i).getTransactionStatus());
				        

			        }
			        
			        }
			        else
			        {
			            System.out.println("Failed to get transaction list:  " + getResponse.getMessages().getResultCode());
			        }
			}
			
		}catch (Exception e) {
			 e.printStackTrace();
		}
	}
}
