package com.elms.serviceImp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elms.exception.BasicExceptionHandler;
import com.elms.model.Activity;
import com.elms.model.EmailDetails;
import com.elms.model.EmailTemplateAdminForm;
import com.elms.model.Fee;
import com.elms.repository.CommonRepository;
import com.elms.service.CommonService;


@Service
public class CommonServiceImpl implements CommonService {

	private static final Logger logger = LogManager.getLogger(CommonServiceImpl.class);

	@Autowired
	CommonRepository commonRepository;

	
	/**
	 * This method is to check entered values Business Account Number and 
	 * Renewal Code (activity Id) are valid combination. 
	 * If the combination exists in BL_ACTIVITY / BT_ACTIVITY tables 
	 * then it will return the type of renewal user trying to renew.
	 * 
	 * @return renewalCode String Object
	 * @Parameters String
	 */
	@Override
	public String checkRenewalType(String renewalCode,String businessActNo) throws BasicExceptionHandler{		
		String permitNumber = commonRepository.checkRenewalType(renewalCode,businessActNo);		
		logger.debug("return value permitNumber :: " +permitNumber);
		return permitNumber;
	}
	
	/**
	 * This method is to check if the Fee for BL/BT is payed or the Renewal application 
	 * is submitted based on the renewal code or activity Id.
	 * @param String
	 * @throws Exception
	 * @return String 
	 */
	@Override
	public String getBlOrBtActivityRenewalOnlineFlag(Activity activity) throws BasicExceptionHandler{
		String renewalOnline = commonRepository.getBlOrBtActivityRenewalOnlineFlag(activity);		
		logger.debug("return value renewalOnline :: " +renewalOnline);
		return renewalOnline;
	}	

	/**
	 * This method is to fetch Fee list for BL/BT after payment of the Renewal application 
	 * based on the renewal code or activity Id.
	 * @param String, String
	 * @throws Exception
	 * @return List<Fee>
	 * 
	 */
	@Override
	public List<Fee> getFeesFromTempActFees(String actId, String tempId) throws BasicExceptionHandler{
		List<Fee> feeList = new ArrayList<Fee>();
		feeList = commonRepository.getFeesFromTempActFees(actId, tempId);		
		logger.debug("return value feeList :: " +feeList.size());
		return feeList;
	}
	
	/**
	 * This method is to fetch Fee Account for each Fee id that is passed as variable to this method after payment of the Renewal application 
	 * based on the fee Id.
	 * @param String, String
	 * @throws Exception
	 * @return List<Fee>
	 * 
	 */
	@Override	
	public String getFeesAccountForFeeId(String feeId) throws BasicExceptionHandler{
		String feeAccount = "";
		feeAccount = commonRepository.getFeesAccountForFeeId(feeId);		
		logger.debug("return value feeAccount :: " +feeAccount);
		return feeAccount;
	}

	/**
	 * This method is to check if the payment is fully done or partially done for BL/BT Renewal application 
	 * is submitted based on the renewal code or activity Id.
	 * @param String
	 * @throws Exception
	 * @return String 
	 */

	@Override
	public double getFullOrPartialPaymentFlag(Activity activity) throws BasicExceptionHandler {
		double partialPayFlag = commonRepository.getFullOrPartialPaymentFlag(activity);		
		logger.debug("return value partialPayFlag :: " +partialPayFlag);
		return partialPayFlag;
	}

	/**
	 * This method is to download attachments in the same page as a part of BL renewal
	 * @param req
	 * @param resp
	 */
	@Override
	public void downloadAttachment(HttpServletRequest request, HttpServletResponse response) throws BasicExceptionHandler{
		
		String fileName=request.getParameter("fileName"); 
		String filePath =request.getParameter("filePath");
		
		response.setContentType("text/html");		
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-Disposition", "attachment; filename=\""+fileName + "\".pdf");		
		
		InputStream in = null;		
		
		try {
			  URL url = new URL(filePath);
			  in = url.openStream();			  
			  byte[] buffer = new byte[10240];

			  try (
			      OutputStream output = response.getOutputStream();
			  ) {
			      for (int length = 0; (length = in.read(buffer)) > 0;) {
			          output.write(buffer, 0, length);
			      }
			  }
			 
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(in != null) {
					in.close();
				}				
			}catch(Exception e) {
				
			}
		}	
		
	}

	/**
	 * This method is to restrict the user login based on lkup_act_type online flag
	 * @param activityForm
	 * @return
	 */
	@Override
	public String getActTypeOnlineFlag(Activity activityForm) throws BasicExceptionHandler{
		String actTypeOnlineFlag = commonRepository.getActTypeOnlineFlag(activityForm);		
		logger.debug("return value actTypeOnlineFlag :: " +activityForm);
		return actTypeOnlineFlag;
	}
	
	/**
	 * This method is to update the TEMP_ACTIVITY_DETAILS Table for every next button click.
	 * @param activity
	 * @throws BasicExceptionHandler
	 * @return 
	 * 
	 */
	@Override
	public void updateUserDetails(Activity activity) throws BasicExceptionHandler {
		commonRepository.updateUserDetails(activity);		
	}
	
	/**
	 * This method is to fetch the LKUP_SYSTEM Table in the form of single Map object 
	 * having key- value pairs from List of Map objects
	 * @param 
	 * @throws Exception
	 * @return Map<String,String>
	 * 
	 */
	public Map<String, String> getLkupSystemDataMap() throws BasicExceptionHandler{
		Map<String, String> lkupSystemDataMap = new HashMap<String,String>();
		lkupSystemDataMap = commonRepository.getLkupSystemDataMap();
		return lkupSystemDataMap;        
	}
	
	
	/**
	 * This method is to remove duplicate entries of same combination of Business Acc no & Renewal Code from 
	 * TEMP_ACTIVITY_DETAILS Table. And also it insert a new record with all other details that user enters. 
	 * having key- value pairs from List of Map objects
	 * @param Activity Object
	 * @throws BasicExceptionHandler
	 * @return Activity Object 
	 */
	@Override
	public Activity saveActivity(Activity activity) throws BasicExceptionHandler {
		activity = commonRepository.saveActivity(activity);
		return activity;
	}

	/**
	 * To reset upload attachments
	 */
	@Override
	public void resetBlUploadAttachments(String tempId) {
		commonRepository.resetBlUploadAttachments(tempId);
		
	}

	/**
	 * This method is to fetch the Email Templates based on email type
	 * @param emailDetails
	 * @throws 
	 * @return EmailTemplateAdmin Form 
	 */
	@Override
	public EmailTemplateAdminForm getEmailData(EmailDetails emailDetails) throws BasicExceptionHandler{
		EmailTemplateAdminForm emailTemplateAdminForm = commonRepository.getEmailData(emailDetails);
		return emailTemplateAdminForm;
	}
	
	@Override
	public void savePaymentLog(HttpServletResponse response, Map<String, String> datamap) throws IOException {
		JSONObject jsonObj = commonRepository.savePaymentLog(datamap);
		response.setStatus(200);
		response.setContentType("application/json");
		response.getWriter().write(jsonObj.toString());
	}
	
	@Override
	public double getPaidAmount(Activity activity) throws BasicExceptionHandler {
		double partialPayFlag = commonRepository.getPaidAmount(activity);		
		logger.info("return value getPaidAmount :: " +partialPayFlag);
		return partialPayFlag;
	}
}