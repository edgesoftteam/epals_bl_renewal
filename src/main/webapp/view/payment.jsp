<%@page import="java.util.Calendar"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="com.elms.util.StringUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="com.elms.model.*,com.elms.common.Constants,com.elms.util.StringUtils,java.util.List,com.elms.model.Fee" %> 
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="logic"%>     
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
String contextRoot = request.getContextPath();
String scrnNameForBack = Constants.PAYMENT_DETAILS_SCREEN;
int year = Calendar.getInstance().get(Calendar.YEAR)  % 100;
%>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">	
<head>  
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/es_styles.css"/>  
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css"  href="css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="css/renewal.css" /> 
    <link rel="stylesheet" type="text/css" href="css/main-ext.css" /> 
    <script src="js/jquery/3.5.1/jquery.min.js"></script>
    <script src="js/font-awesome-script.js"></script>
    <script src="js/renewal.js"></script>  
         
    <script type="text/javascript" src="js/bootstrap/bootstrap.bundle.min.js"></script>
	<script src="vendor/bootstrap/js/logout-timer.js"></script>
	<script src="js/popper/popper-1.12.9.min.js" ></script>
	<script src="js/bootstrap/4.0.0/js/bootstrap-4.0.0.min.js"></script>	    
    
	<script>
	
	$(document).on('contextmenu', function () {
		return false;
	});
	</script>
	<script src="js/jquery-1.12.4.min.js"></script>
	<script type="text/javascript">
	
	$(document).ready(function() {	
		window.setInterval("LogoutTimer(0)", 1000);
	});
	
	function back(){    	 
		document.forms[0].action="${pageContext.request.contextPath}/redirectToBlOrBtForBack";
		document.forms[0].submit();
	}
	
	function cancel(){
		 document.forms[0].action = "${pageContext.request.contextPath}/cancelSession";
	     document.forms[0].submit();
	}
	function validateForm(){
		
		
		if(document.forms[0].cardNumber.value==''){
			  alert('Card Number is a required field');
	    	  document.forms[0].cardNumber.focus();
	    	  return false;
	    }else if(document.forms[0].expMonth.value=='MM'){
	    	  alert('Expiration Month is a required field');
	    	  document.forms[0].expMonth.focus();
	    	  return false;
	    	
	    }else if(document.forms[0].expYear.value=='YY'){
	    	  alert('Expiration Year is a required field');
	    	  document.forms[0].expYear.focus();
	    	  return false;
	    	
	    }else if(document.forms[0].cvv.value==''){
	    	  alert('Cvv is a required field');
	    	  document.forms[0].cvv.focus();
	    	  return false;
	    	
	    }else if(document.forms[0].cardHolderName.value==''){
	    	  alert('Name  is a required field');
	    	  document.forms[0].cardHolderName.focus();
	    	  return false;
	    	
	    }else{		
		document.forms[0].elements['Pay'].disabled = true;
		document.forms[0].action="${pageContext.request.contextPath}/redirectToBlOrBt";
		document.forms[0].submit();
	    }
	}
	function cvv() {
	     window.open("<%=contextRoot%>/images/cvv.jpg","_blank", "titlebar=no, location=no, toolbar=no, scrollbars=no, resizable=no, width=320, height=235");
	     return false;
	}
	</script>
	<style>
	#inspection-info {
	margin-left: 10% !important;
	}
	
	.form-group {
	margin: 0 !important;
	}
	</style>
    <title>City of Burbank Business License  / Business Tax Renewal - Quantity</title>
</head> 
<body class="business-renewal-portal-page" style="zoom: 75%;" >
    <div class="portal-page-header bg-light">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-4 col-sm-3 col-xl-2 pe-0">
                    <nav class="navbar navbar-light">
                        <div class="container p-0 m-0">
                            <a class="navbar-brand" href="#">
                                <img src="images/logo-color.png" alt="" width="214" height="auto">
                            </a>
                        </div>
                        
                    </nav>
                </div>
              <!--   <div align="center">
                 CITZEN ACCESS PORTAL
            	</div> -->
            </div>
        </div>
    
    
<form:form name="paymentForm" cssClass="login100-form validate-form p-b-33 p-t-5" modelAttribute="activityForm" method="post" class="form-horizontal">

    <div class="container-row-fluid">
        <div class="row align-items-start m-0">
            <div class="col-md-3 menu-col pt-5 px-3 vh-100 menu-contents menu-item">
                <div class="bg-white py-4 mb-1 px-2">
                    <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4">Bill No:<span class="fw-bold text-darkblue ps-2">${activityDetails.permitNumber}</span></p>
                </div>
                 <div class="bg-white py-4 mb-1 px-2">
                    <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4">Amount:<span class="fw-bold text-darkblue ps-2">
				<strong>
			 	   											$
					                                <logic:choose>
										 	   	 		<logic:when test="${activityDetails.totalFee gt 0.0} && ${not fn:contains(activityDetails.totalFee,'.')}">
										 	   	 			${activityDetails.totalFee gt 0.0}.00
										 	   	 		</logic:when>
										 	   	 		<logic:otherwise>
										 	   	 			${activityDetails.totalFee}
										 	   	 		</logic:otherwise>
										 	   	 	</logic:choose>
											 	 </strong>
				</span></p>
                </div>
                <div class="bg-white py-4 business-info mb-1">
               		<div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">Business Name :</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 d-block fw-normal text-989898 text-uppercase">${activityDetails.businessName}</span>
                    </div>
                    <div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">Business Account No :</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 d-block fw-normal text-989898 text-uppercase">${activityDetails.businessAccNo}</span>
                    </div>
                    <div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">Business Type :</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 d-block fw-normal text-989898 text-capitalize">${activityDetails.description}</span>
                    </div>
                    <div class="pb-2 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">Business Email :</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 fw-normal d-block text-989898">
                    
                     <logic:choose>
				       	<logic:when test="${not empty activityDetails.tempEmail}"> ${activityDetails.tempEmail}</logic:when>
				       	<logic:otherwise> ${activityDetails.email}	</logic:otherwise>
				    </logic:choose>
				    
				    </span>
                    </div>
                </div>
                 
                <div class="bg-white py-4 business-info mb-1">
                    <div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">					      
                        			<strong>Business Address : </strong></td>
					      	</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 fw-normal text-989898 d-block text-uppercase">
                         <%--  ${activityDetails.strNo}&nbsp;${activityDetails.address}&nbsp;${activityDetails.unit} --%>
                         ${activityDetails.businessStrName}
                         <br>
                         ${activityDetails.businessCityStateZip}

                         

                        </span>

                    </div>

                </div>
                
<logic:if test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.mailStrNo) or (not empty activityDetails.mailStrName)}">
					      	
                <div class="bg-white py-4 business-info mb-1">
                    <div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">					      
                        	<logic:if test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.mailStrNo) or (not empty activityDetails.mailStrName)}">
					      		<td width="30%"><strong>Business Mailing Address : </strong></td>
					      	</logic:if>
					      	</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 fw-normal text-989898 d-block text-uppercase">
                           <logic:if test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.mailStrNo) or (not empty activityDetails.mailStrName)}">
    					      	<%-- ${activityDetails.mailStrNo} ${activityDetails.mailStrName}<br>
	    				      	${activityDetails.mailCity} ${activityDetails.mailState} ${activityDetails.mailZip} --%>
	    				      		${activityDetails.strNo} ${activityDetails.address} ${activityDetails.unit}<br>
	    				      	${activityDetails.city} ${activityDetails.state} ${activityDetails.zip}
    				       </logic:if>   

                        </span>  

                    </div>

                </div>
                </logic:if>
            </div>
            <div class="col-md-9 data-col bg-white content-item">
                <div class="modify-data-page-content modify-data" id="modify-content">
                    <div class="container-fluid px-0 edit-profile  sec-1">
                        <div class="edit-profile-header pb-3">
                            <h3 class="section-header">city of burbank community development department

			                <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BL')}">
						        Business License
						    </logic:if>
						    <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BT')}">
						        Business Tax
						    </logic:if>
			     				Online Portal
                           </h3>
                        </div>
                        <div class="pt-3 license-renewal-content-wrap">
                            <div class="container-fluid px-0 edit-profile-form pt-2 sec-1">
                                

                                            
                                            
                                    </div>
                                
                            </div>
                            
                            
                                                  
		<div class="row m-0 p-0 align-items-center justify-content-between" style="max-width: 1100px;"  style="align:left" >
 <div class="form-group row">		
		
		                                    <table class="table tabel-sm">
                                        <thead>
                                            <tr class="bg-darkblue">
                                              <th width="25%" class="align-middle text-start px-3 px-xl-4 py-3 font-medium font-roboto fs-18 lh-sm text-white">Credit Card Payment</th>
                                              <th width="73%"></th>
                                              <th width="2%" class="align-middle text-start px-3 px-xl-4 py-3 font-medium font-roboto fs-18 lh-sm text-white" align="right">
                                              <strong>
			 	   											$
					                                <logic:choose>
										 	   	 		<logic:when test="${activityDetails.totalFee gt 0.0} && ${not fn:contains(activityDetails.totalFee,'.')}">
										 	   	 			${activityDetails.totalFee gt 0.0}.00
										 	   	 		</logic:when>
										 	   	 		<logic:otherwise>
										 	   	 			${activityDetails.totalFee}
										 	   	 		</logic:otherwise>
										 	   	 	</logic:choose>
											 	 </strong>
                                              </th>
                                            </tr>
                                        </thead>
                                        </table>
                                        
                                         <div class="form-inner-wrap px-2 py-2">
                                        <div class="row align-items-start justify-content-between">
                                             <div class="row">
                                        <div class="col-md-6">
												<div class="col-12 col-lg-12 mb-1 form-field">
													 <label for="updateworkphone" class="fs-18 lh-sm text-start fw-bold form-label mb-2 text-capitalize">Card Number</label>
													<div class="input-group">
														<!-- <html:text name="paymentForm" property="cardNumber" styleId="cardNumber" maxlength="16" style="width: 50%" styleClass="form-control" onkeypress="check();"></html:text> --> 
															<input type="text" class="fs-18 text-start form-control text-left bg-white" placeholder="Card Number"
															name="cardNumber" style="width: 50%"  maxlength="16" class="form-control" onkeypress="return event.charCode >= 47 && event.charCode <= 57">
													</div>
												</div>
											</div>
											<div class="col-md-3">
											
												<div class="mb-0 form-field">
													<label class="fs-18 lh-sm text-start fw-bold form-label mb-2 text-capitalize">Expiration
													</label>
													<div class="input-group">
													<%-- <html:text name="paymentForm" property="expMonth" styleId="expMonth" maxlength="2" style="width: 45%" styleClass="form-control" onkeypress="check();"></html:text> --%>
													<select name="expMonth" style="width: 35%" class="form-control">
													<option value="MM">MM</option>
													<%for(int i=1; i<13; i++){ 
													String month ="0";
													if(i>9){
														month=""+i;
													}else{
														month=month+i;
													}
													%>
													<option value="<%=month %>"><%=month%></option>
													<%} %>
													</select>
												&nbsp;	
												
												<select name="expYear"  class="form-control" style="width: 35%">
												<option value="YY">YY</option>
												<%
												for(int j=year; j<year+11; j++){ 
													String y=""+j;
												%>
												<option value="<%=y%>"><%=y%></option>
													<%} %>
												</select>
												<%-- <html:text name="paymentForm" property="expYear" styleId="expYear" maxlength="2" style="width: 40%" styleClass="form-control" onkeypress="check();"></html:text> --%>
														<!-- <input type="number" class="form-control" placeholder="MM"
															name="expMonth" style="width: 45%"  maxlength="2"/> &nbsp;
															<input	type="number" class="form-control" placeholder="YY"
															name="expYear" style="width: 40%" required="required" maxlength="2"/> -->
													</div>
												</div>
											</div>
											<div class="col-md-3">
												<div class="mb-0 form-field">
													<label for="business-Cvv" class="fs-18 lh-sm text-start fw-bold form-label mb-2 text-capitalize"> Cvv &nbsp; <a href="javascript:void(0)"
														onclick="javascript:cvv()" style="cursor: pointer"><span
															class="fa fa-question-circle"></span></a>
													</label>
													<div class="input-group">
													<!-- <html:text name="paymentForm" property="cvv" styleId="cvv" maxlength="4"  styleClass="form-control" onkeypress="check();"></html:text> -->
													<input type="text" class="form-control" 
														placeholder="CVV" name="cvv" maxlength="4" onkeypress="return event.charCode >= 47 && event.charCode <= 57">
														</div>
												</div>
											</div>
										</div>
                                            
                                             <div class="row">
                                        <div class="col-md-6">
												<div class="col-12 col-lg-12 mb-1 form-field">
													 <label for="business-Name" class="fs-18 lh-sm text-start fw-bold form-label mb-2 text-capitalize">First Name</label>
													<div class="input-group">
														<!-- <html:text name="paymentForm" property="firstName" styleId="firstName" style="width: 50%" styleClass="form-control" onkeypress="return lettersOnly()"></html:text> -->
															<input type="text" class="form-control" placeholder="First Name"
															name="cardHolderName" style="width: 50%"  required="required" />
													</div>
												</div>
											</div>
											
											<div class="col-md-6">
											<div class="col-12 col-lg-12 mb-1 form-field">
													 <label for="updateworkphone" class="fs-18 lh-sm text-start fw-bold form-label mb-2 text-capitalize">Last Name</label>
													<div class="input-group">
													<input type="text" class="form-control" name="cardHolderLastName" placeholder="Last Name" style="width: 50%" />
															<!-- <input type="text" form="paymentForm" class="form-control" placeholder="Last Name"
															name="lastName" style="width: 50%" required="required" /> -->
													</div>
												</div>
											</div>
											
											</div>
                                            
                                             <div class="row">
                                        <div class="col-md-6">
												<div class="col-12 col-lg-12 mb-1 form-field">
													 <label for="updateworkphone" class="fs-18 lh-sm text-start fw-bold form-label mb-2 text-capitalize">Provide Billing Address</label>
													<div class="input-group">
														<!-- <html:text name="paymentForm" property="address" styleId="address" maxlength="50"  style="width: 50%" styleClass="form-control" ></html:text> --> 
															<input type="text"  class="form-control" placeholder="address"
															name="payeeAddress" style="width: 50%" maxlength="50"/>
													</div>
												</div>
											</div>
											<div class="col-md-3">
											
												<div class="mb-0 form-field">
													<label class="fs-18 lh-sm text-start fw-bold form-label mb-2 text-capitalize">&emsp;
													</label>
													<div class="input-group">
													<!-- <html:text name="paymentForm" property="city" styleId="city"  maxlength="10" style="width: 45%" styleClass="form-control" ></html:text>&nbsp;
													<html:text name="paymentForm" property="state" styleId="state" maxlength="2"  style="width: 40%" styleClass="form-control" ></html:text> -->
														<input type="text" class="form-control" placeholder="city"
															name="payeeCity" style="width: 45%"   maxlength="10"/> &nbsp;
															<input	type="text" class="form-control" placeholder="state"
															name="payeeState" style="width: 40%" required="required" maxlength="2"/>
													</div>
												</div>
											</div>
											<div class="col-md-3">
												<div class="mb-0 form-field">
													<label class="fs-18 lh-sm text-start fw-bold form-label mb-2 text-capitalize">  &emsp; 
													</label>
													<div class="input-group">
													<!-- <html:text name="paymentForm" property="zip" styleId="zip" maxlength="5" style="width: 40%" styleClass="form-control" onkeypress="check();"></html:text> -->
													<input type="text" class="form-control" required="required"
														placeholder="zip" name="payeeZip" maxlength="5" onkeypress="return event.charCode >= 47 && event.charCode <= 57"/>
														</div>
												</div>
											</div>
										</div>
                                            
                                            </div></div>
                                        
                                     
                                </div>
                                
                              
                                
                                </div>
                                </div>
    	<div class="row m-0 p-0 align-items-center justify-content-between" style="max-width: 1100px;"  style="align:left" >
    
				
                                
                                  <div class="container-fluid px-0 license-renewal-buttons pt-0 pb-3">
		                        <div class="lncv-park-inner pt-0">
		                            <div class="modify-content-btn pt-4 text-center">
		                              <p style="text-align: center;color: red;">2% Transaction Fee is part of the Total Payment.</p>
                    <p style="text-align: center;color: red;">Please wait while we are processing your payment. Do not hit refresh or back button.</p>
		                                <button type="button" class="btn btn-primary license-renewal-back mt-2 text-uppercase px-4 me-3" onclick="history.back();">back</button>
		                                <button type="button" class="btn btn-primary license-renewal-next mt-2 text-uppercase px-4" id="Pay" onclick="validateForm();"> Pay</button>
		                                <a type="button" data-bs-toggle="modal" data-bs-target="#documents-submit-msg" class="btn btn-cancel license-renewal-cancel mt-2 px-0 ms-3 text-uppercase text-darkblue" onclick="cancel();">cancel</a>
		                            </div>
	<input type="hidden" name="renewalCode" value="${activityDetails.renewalCode}">
	<input type="hidden" name="permitNumber" value="${activityDetails.permitNumber}">
	<input type="hidden" name="businessAccNo" value="${activityDetails.businessAccNo}">
	<input type="hidden" name="tempId" value="${activityDetails.tempId}">
    <input type="hidden" name="actType" value="${activityDetails.actType}">
    <input type="hidden" name="totalFee" value="${activityDetails.totalFee}">
	<input type="hidden" name="screenName" value="<%=StringUtils.nullReplaceWithBlank(scrnNameForBack)%>">
		                            
		                        </div>
                           </div>
                                
                                
			                                </div>
			                            
			                            
                        </div>
                    </div>
                    
                    
                  

                </div>
            </div>
            </form:form>
        </div>

 				

				
 				
				
 
    </div>
</body>
</html>
