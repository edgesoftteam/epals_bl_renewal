<%@page import="com.elms.common.Version"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
<%@ page import="com.elms.model.*,com.elms.util.StringUtils" %> 
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="logic"%> 
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css" />
    <link href="css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="css/renewal.css" /> 
    <script src="js/jquery/3.5.1/jquery.min.js"></script>
    <script src="js/font-awesome-script.js"></script>
    <script src="js/renewal.js"></script>           
    <script type="text/javascript" src="js/bootstrap/bootstrap.bundle.min.js"></script>
    <title>Business License-Tax Renewal Login</title>
    
    <script>
$(document).on('contextmenu', function () {
   // $('#exampleModal').modal('show');

	return false;
});
</script>
<script>
function checkForm(form){
	var flag = true;
	var fieldName = "";
	if(document.forms[0].elements['businessAccNo'].value == "") {
		document.forms[0].elements['businessAccNo'].focus();
		fieldName = "Account Number";
		flag = false;
	}else if(document.forms[0].elements['renewalCode'].value == "") {
		document.forms[0].elements['renewalCode'].focus();
		fieldName = "Renewal Code";
		flag = false;
	}	
	
	if(flag == false) {
		alert("Please enter " + fieldName);
		return false;
	}
	
	if (!$('#termsAndConditions').is(':checked')) {
	  alert('Please indicate that you accept the Terms and Conditions');
	  return false;
	}else{
		document.forms['permitRenewalDetails'].action = "${pageContext.request.contextPath}/verifyRenewalType";
		document.forms['permitRenewalDetails'].submit();
	}
}
	
function openTerms(){
	document.forms[0]["termsAndConditions"].checked=true;	
}

function sessionClear(){		
	var tempId = '<%=request.getSession().getAttribute("tempId")%>';
	if(tempId=="null"){
		tempId="0";
		document.forms[0].action = "${pageContext.request.contextPath}/cancelSession";
		document.forms[0].submit();
	}
	if(tempId=="0"){
		document.forms[0].businessAccNo.value = '';
		document.forms[0].renewalCode.value = '';
	}
	return true;
}
</script>

<style>

ul.circle {
  list-style-type: circle;
}
ul.a {
  list-style-type: round;
}
body .modal-ku {
	width: 750px;
}

@media ( min-width : 1200px) {
	.modal-xlg {
		width: 90%;
	}
}

.modal-dialog {
	width: 660px;
}

.modal-header {   
	background-color: #337AB7;
	padding: 16px 16px;
	color: #FFF;
	border-bottom: 2px dashed #337AB7;
}

.center {
  margin: auto;
  width: 60%;
  padding: 10px;
}

.modal-content { 

height: 450px !important; 

overflow: scroll !important;  

} 
</style>

</head>
<body class="business-login-page-v2 position-relative"  style="overflow-y: hidden;">

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-footer">
</div>
</div>
</div>

<script src="js/popper/popper-1.12.9.min.js" ></script>
<script src="js/bootstrap/bootstrap-4.0.0.min.js"></script>

					
    <nav class="navbar navbar-light px-0 position-absolute top-0 start-0 pt-3">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="images/logo-color.png" alt="" width="214" height="auto">
            </a>
        </div>
    </nav>
    <div class="row justify-content-start">
        <div class="form-container col-md-6 d-flex justify-content-center align-items-center">
            <div class="login-page-v2">

<html:form name="permitRenewalDetails"  cssClass="login100-form validate-form p-b-33 p-t-5"  modelAttribute="loginForm" method="post" autocomplete="off" onload="return sessionClear();">
    <spring:url value="/blActivityDetails" var="loginURL" />
    
    <font color="red">&nbsp;${activityDetails.displayErrorMsg}</font>
	<logic:if test="${fn:containsIgnoreCase(activityDetails.attachFlag,'Y')}">
		<div class=" p-t-10 p-b-10">
			<br/><strong><a target="_blank" href="${pageContext.request.contextPath}/attachReUpload?renewalCode=${activityDetails.renewalCode}&businessAccNo=${activityDetails.businessAccNo}" class="text-primary" style="margin-left: 10px;">Click here to upload document(s)</a></strong>
		</div>
	</logic:if>
					

                <logic:choose>
			
					<logic:when test="${not empty activityDetails.tempId}">	
                    <div class="pb-3 form-field">
                        <label for="account-number" class="fs-18 d-inline-block fw-bold lh-base form-label pb-2 m-0">Account Number</label>
                        <input type="text" id="businessAccNo" type="text" name="businessAccNo" placeholder="Account Number"  autocomplete="off" onfocus="this.value=''" class="fs-18 fw-normal lh-base form-control text-left bg-white">
                    </div>
                    <div class="pb-4 form-field">
                        <label for="renewal-code" class="fs-18 d-inline-block fw-bold lh-base form-label pb-2 m-0">Renewal Code</label>
                        <input type="text" class="fs-18 fw-normal lh-base form-control text-left bg-white" id="renewalCode" type="text" name="renewalCode" placeholder="Renewal Code" maxlength="10" autocomplete="off" onfocus="this.value=''">
                    </div>
                   
                    </logic:when>
                    
                    <logic:otherwise>                    
                    <div class="pb-3 form-field">
                        <label for="account-number" class="fs-18 d-inline-block fw-bold lh-base form-label pb-2 m-0">Account Number</label>
                        <input type="text" placeholder="" class="fs-18 fw-normal lh-base form-control text-left bg-white" id="businessAccNo" type="text" name="businessAccNo" placeholder="Account Number" value="${activityDetails.businessAccNo}"
							 autocomplete="off" onfocus="this.value=''" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                    </div>
                    <div class="pb-4 form-field">
                        <label for="renewal-code" class="fs-18 d-inline-block fw-bold lh-base form-label pb-2 m-0">Renewal Code</label>
                        <input type="text" placeholder="" class="fs-18 fw-normal lh-base form-control text-left bg-white" id="renewalCode" type="text" name="renewalCode" placeholder="Renewal Code" maxlength="10" value="${activityDetails.renewalCode}"
							autocomplete="off" onfocus="this.value=''" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                    </div>

                    </logic:otherwise>
                    
                </logic:choose>

                    <div class="col-12 col-lg-12 mb-2 form-field p-0">
                        <input class="form-check-input mt-0" type="checkbox" id="termsAndConditions">
                         <label for="agreed-terms-condition" class="fs-20 align-middle w-90 pe-5 ps-1 text-darkblue font-roboto fs-6 lh-sm text-start fw-bold form-label mb-3">
                        <a href="#" onclick="openTerms();" data-toggle="modal" data-target="#myModal" class="text-primary" style="text-decoration:underline;">I have read and agree to the terms.
                        </a></label>
                    </div>
                                    
    
                    <div class="text-start pt-1">
                        <a type="submit" href="#" onclick="return checkForm(this);" class="btn btn-primary login-btn mt-0 fw-bold text-white fs-6 text-uppercase px-5">next</a>
                    </div>
                  
                </html:form>
                 <div align="right" style="margin-bottom: -100px; margin-right: -190px;" >
                   <label class=""> <br><br><br> V: <%=Version.number %></label>
        	</div>
            </div>
          
           
        </div>
        
        <div class="content-container row align-items-center bg-image col-md-6" style="background-image: url('images/background/login-page-v2-blue.jpg');min-height: 125vh;">
            <div class="login-page-v2-content">
                <div class="row align-items-center ps-5">
                    <h1 class="fw-normal text-start text-capitalize text-white">Business License / Business Tax Renewal Portal</h1>
                    
                   <!--  <p class="fw-normal text-start text-white mb-0 fs-5 lh-base">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p> -->
                </div>
            </div>
        </div> 
       
                      		<!-- The Modal -->
		<div class="modal fade" id="myModal">
			<div class="modal-dialog modal-dialog-centered modal-lg">
				<div class="modal-content">

					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title">TERMS AND CONDITIONS FOR ONLINE PAYMENTS </h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div> 

					<!-- Modal body -->
					<div class="modal-body modal-lg" style="font-family: calibri;">
				<p style="font-size:15px;font-family: Sans-serif;">


 
 <div class="container">
 	<br/><font color="#294B96"><b>Introduction:</b> </font>These terms and conditions apply to the User who uses the Online Services provided for any payment made to City of Burbank (City). Kindly read these terms and conditions carefully. By authorizing a payment to City through the online payment service ("the service"), it would be treated as a deemed acceptance to these terms and conditions. City reserves all the rights to amend these terms and conditions at any time without giving prior notice. It is the responsibility of the User to have read the terms and conditions before using the Service.

	<br/><br/><font color="#294B96"><b>Key terms:</b> </font>The following is a summary of the key terms of this service:
  <ul style="list-style-type: square; list-style-position: inside;" >
 <li style="list-style-type: square">&nbsp;Payment(s) through this Service may only be made with a Credit Card or Debit card.</li>
 <li style="list-style-type: square">&nbsp;Before using this Service, it is recommended that the user shall make necessary enquiry about the charges or fees payable against the Credit/Debit card used from Credit Card or the Debit Card service provider i.e. the respective Bank.</li>
 <li style="list-style-type: square">&nbsp;The credit card information supplied at the time of using the service is processed by the payment gateway of the service provider and is not supplied to City. It is the sole responsibility of the User of the service to ensure that the information entered in the relevant fields are correct. It is recommended that you take and retain a copy of the transaction for record keeping purposes, which might assist in resolution of any disputes that may arise out or usage of the service</li>
 <li style="list-style-type: square">&nbsp;The User agrees, understands and confirms that his/ her personal data including without limitation details relating to debit card/ credit card/net banking transmitted over the Internet may be susceptible to misuse, hacking, theft and/ or fraud and that City or the Payment Service Provider(s) have no control over such matters.</li>
 <li style="list-style-type: square">&nbsp;The service is provided using a payment gateway service provider through a secure website. However, neither the payment gateway service provider nor the City gives any assurance, that the information so provided online by a user is secured or may be read or intercepted by a third party. City does not accept or assume any liability in the event of such unauthorized interception, hacking or other unauthorized access to information provided by a user of the service.</li>
 <li style="list-style-type: square">&nbsp;City and/or the Payment Service Providers shall not be liable for any inaccuracy, error or delay in, or omission of (a) any data, information or message, or (b) the transmission or delivery of any such data, information or message; or (c) any loss or damage arising from or occasioned by any such inaccuracy, error, delay or omission, non-performance or interruption in any such data, information or message. Under no circumstances shall the City and/or the Payment Service Providers, its employees, directors, and its third party agents involved in processing, delivering or managing the Services, be liable for any direct, indirect, incidental, special or consequential damages, or any damages whatsoever, including punitive or exemplary arising out of or in any way connected with the provision of or any inadequacy or deficiency in the provision of the Services or resulting from unauthorized access or alteration of transmissions of data or arising from suspension or termination of the Service.</li>
 <li style="list-style-type: square">&nbsp;The User agrees that City or any of its employees will not be held liable by the User for any loss or damages arising from your use of, or reliance upon the information contained on the Website, or any failure to comply with these Terms and Conditions where such failure is due to circumstance beyond City's reasonable control.</li>
 </ul>
 </div>
<br/><font color="#294B96"><b>Debit/Credit Card, Bank Account Details:</b></font>

 <div class="container">
 <ul style="list-style-type: square;list-style-position: inside;">
 <li style="list-style-type: square">The User agrees that the debit/credit card details provided by him/ her for use of the aforesaid Service(s) must be correct and accurate and that the User shall not use a Debit/ credit card, that is not lawfully owned by him/ her or the use of which is not authorized by the lawful owner thereof. The User further agrees and undertakes to Provide correct and valid debit/credit card details.</li>
<li style="list-style-type: square">The User may pay his/ her fees to City by using a debit/credit card or through online banking accounts. The User warrants, agrees and confirms that when he/ she initiates a payment transaction and/or issues an online payment instruction and provides his/ her card / bank details:</li>
<li>
  <ul style="list-style-type: circle; list-style-position: inside;list-style:none; padding: 10px 20px; margin: 10px 10px;" >
 <li style="list-style-type: circle">The User is fully and lawfully entitled to use such credit / debit card, bank account for such transactions;</li>
 <li style="list-style-type: circle">The User is responsible to ensure that the card/ bank account details provided by him/ her are accurate;</li>
 <li style="list-style-type: circle">The User authorizes debit of the nominated card/ bank account for the Payment of fees selected by such User along with the applicable Fees.</li>
 <li style="list-style-type: circle">The User is responsible to ensure that sufficient credit is available on the nominated card/bank account at the time of making the payment to the City.</li>
</ul></li>
</ul>
</div>
<br/><font color="#294B96"><b>No warranty:</b></font><br/>
No warranty, representation or guarantee, express or implied, is given by the City in respect of the operation of the service.

<br/><br/><font color="#294B96"><b>Disclaimer and Limitation of liability:</b></font><br/>
City does not accept liability for any damage, loss, cost (including legal costs), expenses, indirect losses or consequential damage of any kind which may be suffered or incurred by the User from the use of this service.

<br/><br/><font color="#294B96"><b>Governing law:</b></font><br/>
The terms of this Agreement shall be interpreted according to the laws of the State of California. The Parties agree and consent to the jurisdiction of the state and federal courts of competent jurisdiction exclusively in the County of Los Angeles, California.

<br/><br/><font color="#294B96"><b>Dispute Resolution:</b></font><br/>
The Parties agree to meet and confer concerning all claims, disputes or other matters in question between the Parties arising out of or relating to this Agreement or breach thereof prior to the institution of any litigation.

<br/><br/><font color="#294B96"><b>Refund/Cancellation Policy:</b></font><br/>
Refunds of Fees paid will be administered in accordance with the applicable provisions of the Burbank Municipal Code.
<br/><br/><b><font color="#294B96">Privacy Policy:</font><br/><br/>
Information Collected:</b><br/>
The City of Burbank collects two kinds of user information:
<br/><br/><font style="italic"><b>Anonymous information</b></font><br/><br/>
Information that does not identify specific individuals and is automatically transmitted by your browser. This information can consist of:

 <div class="container">
  <ul style="list-style-type: square; list-style-position: inside;" >
<li style="list-style-type: square;">The URL (Uniform Resource Locator or address) of the web page you previously visited.</li>
<li style="list-style-type: square;">The domain names and/or IP addresses which are numbers that are automatically assigned to your computer whenever you are connected to the Internet or World Wide Web.</li>
<li style="list-style-type: square;">The browser version you are using to access the site.</li>
</ul>
</div>
<br/>This information is used to help improve our web site. None of the information can be linked to any individual.
<br/><br/><font style="italic"><b>Personally Identifiable Information (PII)</b></font><br/>
Information that could include:<br/><br/>

 <div class="container">
  <ul style="list-style-type: square; list-style-position: inside;" >
<li style="list-style-type: square;"> Name</li>
<li style="list-style-type: square;"> Address</li>
<li style="list-style-type: square;"> Email address</li>
<li style="list-style-type: square;"> Telephone number</li>
<li style="list-style-type: square;"> Credit/debit card information</li>
</ul>
</div>
<br/>The City will make every reasonable effort to protect your privacy. It restricts access to your personal identifiable information to those employees that will respond to your request. The City does not intentionally disclose any personal information about our users to any third parties inside or outside the City except as required by law.
<br/><br/>The City only collects personally identifiable information that is required to provide service. You can decline to provide us with any personal information on any site on the Internet at any time. However, if you should choose to withhold requested information, we may not be able to provide you with the online services dependent upon the collection of that information.
<br/><br/><b>Access to Personally Identifiable Information</b><br/><br/>
Access to personally identifiable information in public records at state and local levels of government in Burbank is controlled primarily by the California Public Records Act (Government Code Section 6250, et seq.). Information that is generally available under the Public Records Act may be posted for electronic access through the City's web site. While the Public Records Act sets the general policies for access to City records, other sections of California code as well as federal laws also deal with confidentiality issues.
<br/><br/><b>Access to Your Information</b><br/><br/>
Unless otherwise prohibited by state or federal law, rule, or regulation, you will be granted the ability to access and correct any personally identifiable information. We will take reasonable steps to verify your identity before granting access to review or make corrections to your information.  Each City service that collects personally identifiable information will allow the reviewing and updating of that information. See individual requests for information for specifics.
<br/><br/><b>E-mail Addresses</b><br/><br/>
E-mail addresses obtained through the web site will not be sold or given to other private companies for marketing purposes. The information collected is subject to the access and confidentiality provisions of the Public Records Act, other applicable sections of the California code as well as federal laws. E-mail or other information requests sent to the City web site may be maintained in order to respond to the request, forward that request to the appropriate department within the City, communicate updates to the City page that may be of interest to citizens, or to provide the City web designer with valuable customer feedback to assist in improving the site. Individuals can cancel any communications regarding new service updates at any time.

 <div class="container">
  <ul style="list-style-type: square; list-style-position: inside;" >
<li style="list-style-type: square;">No Warranty: The information and materials contained in this site including, graphics, links or other items are provided as on "As Is" and "As Available" basis by the City which organized and tries to provide information accurately and expressly disclaims liability for error or omission in this information and materials. No warranty of any kind, implied, express or statutory shall be given by the City shall not be limited to the warranty of fitness for a particular purpose and freedom from computer virus is given in conjunction with the information and materials.</li>
<li style="list-style-type: square;">Limitation of Liability: In no event, will City be liable for any damage direct or indirect losses or expenses arising in connection with site or use thereof inability to use by any person delay of operation or transaction, computer virus etc.</li>
</ul>
</div>
<br/><font color="#294B96"><b>Security:</b></font><br/><br/>
The City of Burbank is committed to data security and the data quality of personally identifiable information that is either available from or collected by our web site and has taken reasonable precautions to protect such information from loss, misuse or alteration.
<br/><br/>The City operates "secure data networks" protected by industry standard firewalls and password protection systems. Only authorized individuals have access to the information provided by our users.
<br/><br/>When a City application accepts credit cards or any other particularly sensitive information for any of its services, it encrypts all ordering information, such as your name and credit card number, in order to protect confidentiality.
.

<br/><br/><font color="#294B96"><b>Variations to the Terms and Conditions:</b></font><br/>
The City reserves the right to vary these Terms and Conditions from time to time and the current version will be that published on this website.

<br/><br/>
<font color="red">
<!-- <div style="font-color:red;"> -->
We reserve the right to decline the acceptance of an online payment if your account is in default for any reason. The City of Burbank may also make additions/deletions/alteration to the services offered, at its sole discretion. City reserves the right to withdraw the service at any time at its discretion. City retains the right to change the terms and conditions for Online Fees Payments, without any prior notice.
</font>
				</p>

				<!-- Modal footer -->
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Accept</button>
				</div>				                 
            </div>
				</div>


			</div>
		</div>
		<!-- End of Modal -->
                  
		
    </div>  
  <div id="dropDownSelect1"></div>
 

</body>
</html>