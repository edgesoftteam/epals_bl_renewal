<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="logic"%>     
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script src="vendor/jquery/jquery-1.8.3.js"></script> 
 <link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css"/>
 <script type="text/javascript" src="fancybox/jquery.fancybox.js"></script>
 
 <script type="text/javascript">
 function showFancy(bmcDesc){	 
	 $.fancybox.open({
	    href: bmcDesc,
		type: 'iframe',
		width: '100%',
	    height: '100%',
	    autoScale: false,
	    transitionIn: 'none',
	    transitionOut: 'none',
	    fitToView: true,
	    autoSize: false
	});
 }
 </script>
 <style type="text/css">
.fancybox-outer {
 background-color: #f2f3f4; /* or whatever */
}

.fancybox-content {
 border-color: #F0F8FF !important; /* or whatever */
}

.fancybox-skin {
 background-color: #333399 !important; /* or whatever */ 
 padding: 5px !important; 
}

</style>
</head>
<body>
 <div style="padding:10px;"><p style="font-size:20px;color:#212529;font-family: Arial, sans-serif;line-height:170%">${comments}</p></div>
 <logic:forEach var="bmc" items="${bmcDescList}">		
 <h4 style="text-align:center;color:#212529;font-family: Arial, sans-serif;font-size:30px">${bmc.bmcName}&nbsp;&nbsp;<img src="images/info.png" alt="BMC Description" title="BMC Description" height="25" width="25" class="img-responsive" onclick="showFancy('${bmc.bmcLink}')"/></h4>
 </logic:forEach>
 </body>
</html>