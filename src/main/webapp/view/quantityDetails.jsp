<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="com.elms.model.*,com.elms.common.Constants,com.elms.util.StringUtils" %> 
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="logic"%>   
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>     

<%
String scrnNameForBack = Constants.QUANTITY_DETAILS_SCREEN;
%>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">	
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/es_styles.css"/>  
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css"  href="css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="css/renewal.css" /> 
    <link rel="stylesheet" type="text/css" href="css/main-ext.css" /> 
    <script src="js/jquery/3.5.1/jquery.min.js"></script>
    <script src="js/font-awesome-script.js"></script>
    <script src="js/renewal.js"></script>  
         
    <script type="text/javascript" src="js/bootstrap/bootstrap.bundle.min.js"></script>
	<script src="vendor/bootstrap/js/logout-timer.js"></script>
	<script src="js/popper/popper-1.12.9.min.js" ></script>
	<script src="js/bootstrap/4.0.0/js/bootstrap-4.0.0.min.js"></script>	    
    
<script type="text/javascript">

    $(document).ready(function(){
        $('#purpose').on('change', function() {
          if ( this.value == 'Yes'){
            $("#business").show();
          }else{
            $("#business").hide();
          }
        });
        window.setInterval("LogoutTimer(0)", 1000);
    });    
    
    function enableQtyDetails(){
  	  if (document.getElementById("purpose").value=='Yes'){
  		  document.forms[0]["qtyCheckbox"].checked=true;
          $("#business").show();
        } else{  
        	document.forms[0]["qtyCheckbox"].checked=true;
        	qtyCheckbox
          $("#business").hide();
        }
    }

    function back(){    	 
    	document.forms[0].action="${pageContext.request.contextPath}/redirectToBlOrBtForBack";
   		document.forms[0].submit();
    }

    function cancel(){
    	 document.forms[0].action = "${pageContext.request.contextPath}/cancelSession";
         document.forms[0].submit();
    }
    
    function showCheckbox(optionValue){
   	 if(optionValue=="Yes" || document.forms[0]["purpose"].value=="Yes"){
   		document.forms[0]["qtyCheckbox"].checked=true;
   	 }else{
   		document.forms[0].quantity.value="";
   	 }
   }
   
   function clearQty(){
   	 if(document.forms[0]["qtyCheckbox"].checked==false){
   		document.forms[0].quantity.value="";
   	 }else{
   		 return true;
   	 }
   } 
   

   function validateForm(){
     	var pattern = ${activityDetails.linePattern};
//      	alert(pattern );
     	if(pattern != 1){
     	 	if(document.forms[0].purpose.value=="Yes" && document.forms[0]["qtyCheckbox"].checked==true){
     		   	if(document.forms[0].quantity.value==""){
     		      alert("Please enter "+document.forms[0].qtyDesc.value);
     		      document.forms[0].quantity.focus();
     	    	  return false;
     	    	}
     		   	if(isNaN(document.forms[0].quantity.value)){
     		      alert("Please enter valid quantity number");
     		      document.forms[0].quantity.focus();
     	    	  return false;
     		    }    	
             	document.forms[0].action="${pageContext.request.contextPath}/redirectToBlOrBt";
     	   		document.forms[0].submit();
     	   		return true;
     		}else{
             	document.forms[0].action="${pageContext.request.contextPath}/redirectToBlOrBt"; 		
     	   		document.forms[0].submit();
     	   		return true;
     		}    		
     	}else{
     		if(document.forms[0].quantity.value==""){
   		      alert("Please enter "+document.forms[0].qtyDesc.value);
   		      document.forms[0].quantity.focus();
   	    	  return false;
   	    	}
     		if(isNaN(document.forms[0].quantity.value)){
   		      alert("Please enter valid quantity number");
 		      document.forms[0].quantity.focus();
 	    	  return false;
 		    }  
     		document.forms[0].action="${pageContext.request.contextPath}/redirectToBlOrBt"; 		
 	   		document.forms[0].submit();
 	   		return true;
     	}

     }
</script>
<style>
table { 
border-collapse: separate !important;
} 

</style>
    <title>City of Burbank Business License  / Business Tax Renewal - Quantity</title>
</head> 

<body class="business-renewal-portal-page" style="zoom: 75%;" onload="enableQtyDetails()">
    <div class="portal-page-header bg-light">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-4 col-sm-3 col-xl-2 pe-0">
                    <nav class="navbar navbar-light">
                        <div class="container p-0 m-0">
                            <a class="navbar-brand" href="#">
                                <img src="images/logo-color.png" alt="" width="214" height="auto">
                            </a>
                        </div>
                    </nav>
                </div>
              
             
            </div>
        </div>
    
    
<form:form name="qtyDetailsForm" cssClass="login100-form validate-form p-b-33 p-t-5" modelAttribute="qtyForm" method="post">

    <div class="container-row-fluid">
        <div class="row align-items-start m-0">
            <div class="col-md-3 menu-col pt-5 px-3 vh-100 menu-contents menu-item">
                <div class="bg-white py-4 mb-1 px-2">
                    <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4">Bill No:<span class="fw-bold text-darkblue ps-2">${activityDetails.permitNumber}</span></p>
                </div>
                <div class="bg-white py-4 business-info mb-1">
                	<div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">Business Name :</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 d-block fw-normal text-989898 text-uppercase">${activityDetails.businessName}</span>
                    </div>
                    <div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">Business Account No :</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 d-block fw-normal text-989898 text-uppercase">${activityDetails.businessAccNo}</span>
                    </div>
                    <div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">Business Type :</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 d-block fw-normal text-989898 text-capitalize">${activityDetails.description}</span>
                    </div>
                    <div class="pb-2 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">Business Email :</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 fw-normal d-block text-989898">
                    
                     <logic:choose>
				       	<logic:when test="${not empty activityDetails.tempEmail}"> ${activityDetails.tempEmail}</logic:when>
				       	<logic:otherwise> ${activityDetails.email}	</logic:otherwise>
				    </logic:choose>
				    
				    </span>
                    </div>
                </div>
                 
                <div class="bg-white py-4 business-info mb-1">
                    <div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">					      
                        			<strong>Business Address : </strong></td>
					      	</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 fw-normal text-989898 d-block text-uppercase">
                          <%-- ${activityDetails.strNo}&nbsp;${activityDetails.address}&nbsp;${activityDetails.unit} --%>
                          ${activityDetails.businessStrName}
                         <br>
                         ${activityDetails.businessCityStateZip}

                         

                        </span>

                    </div>

                </div>
                
<logic:if test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.mailStrNo) or (not empty activityDetails.mailStrName)}">
					      	
                <div class="bg-white py-4 business-info mb-1">
                    <div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">					      
                        	<logic:if test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.mailStrNo) or (not empty activityDetails.mailStrName)}">
					      		<td width="30%"><strong>Business Mailing Address : </strong></td>
					      	</logic:if>
					      	</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 fw-normal text-989898 d-block text-uppercase">
                           <logic:if test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.mailStrNo) or (not empty activityDetails.mailStrName)}">
    					      	<%-- ${activityDetails.mailStrNo} ${activityDetails.mailStrName}<br>
	    				      	${activityDetails.mailCity} ${activityDetails.mailState} ${activityDetails.mailZip} --%>
	    				      		${activityDetails.strNo} ${activityDetails.address} ${activityDetails.unit}<br>
	    				      	${activityDetails.city} ${activityDetails.state} ${activityDetails.zip}
    				       </logic:if>   

                        </span>  

                    </div>

                </div>
                </logic:if>
            </div>
            <div class="col-md-9 data-col bg-white content-item">
                <div class="modify-data-page-content modify-data" id="modify-content">
                    <div class="container-fluid px-0 edit-profile  sec-1">
                        <div class="edit-profile-header pb-3">
                            <h3 class="section-header">city of burbank community development department 

			                <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BL')}">
						        Business License
						    </logic:if>
						    <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BT')}">
						        Business Tax
						    </logic:if>
			     				Renewal Portal
                           </h3>
                        </div>
                        <div class="pt-3 license-renewal-content-wrap">
                            <div class="container-fluid px-0 edit-profile-form pt-2 sec-1">
                                

                                            
                                            
                                    </div>
                                
                            </div>
                            
                            
                                                  
		<div class="row m-0 p-0 align-items-center justify-content-between" style="max-width: 1100px;"  style="align:left" >
    <div class="form-group row">
		                  	 <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BL')}">
				 
	        	    <table style="align:left">
					 <logic:if test="${not empty activityDetails.qtyDesc && activityDetails.qtyDesc != 'None'}">
				        	 <logic:if test="${not empty activityDetails.linePattern && activityDetails.linePattern ==3 }">
							        <tr>
							        	<td>
									        <div class="col-xs-3">
								          		<label class="fs-5 text-black font-medium font-roboto lh-base text-end pe-4" for="ex2">${activityDetails.line1Desc} &nbsp; ${activityDetails.qtyOther}</label>
									        </div>					        	
							        	</td>
							        </tr>
					        </logic:if>
					        <logic:if test="${not empty activityDetails.linePattern && (activityDetails.linePattern ==3 || activityDetails.linePattern ==2 )}">
							        <tr>
								        <td>
									        <div class="col-xs-3 fs-5 text-black font-medium font-roboto lh-base text-end pe-4">${activityDetails.line2Desc}
									       
											<logic:if test="${not empty activityDetails.line2Desc}">
												<logic:choose>
													<logic:when test="${not empty activityDetails.tempBlQtyFlag && (activityDetails.tempBlQtyFlag == 'Y' || activityDetails.tempBlQtyFlag == 'Yes')}">
														<select name="tempBlQtyFlag" id='purpose' onchange="showCheckbox(this.value);">
															<option value="No">No</option>
															<option value="Yes" selected="selected">Yes</option>				
														</select>
													</logic:when>
													<logic:otherwise>
														<select name="tempBlQtyFlag" id='purpose' onchange="showCheckbox(this.value);">
															<option value="No">No</option>
															<option value="Yes">Yes</option>				
														</select>
													</logic:otherwise>
												</logic:choose>	
											</logic:if>	
											</div>								
										</td>
									</tr>
									<tr>
										<td>
											<div style="display:none;" id="business">
										   		<input type="checkbox" align="left" id="qtyCheckbox"  onclick="return clearQty()"> Number of ${activityDetails.qtyDesc}<font color="red">*</font>
									    	    <input type="text" maxlength="11" size="8" id="quantity" placeholder="${activityDetails.placeholderDesc}" name="noOfEmp" value="${activityDetails.noOfEmp}" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
											</div>  
										</td>
									</tr>
							</logic:if>
					 </logic:if>
					 <logic:if test="${not empty activityDetails.linePattern && activityDetails.linePattern ==1 }">					
						<tr>
							<td class="fs-5 text-black font-medium font-roboto lh-base text-end pe-4">
						   		 ${activityDetails.line3Desc}&nbsp;<font color="red">*</font>
					    	    <input type="text" maxlength="11" size="8" id="quantity" placeholder="${activityDetails.placeholderDesc}" name="noOfEmp" value="${activityDetails.noOfEmp}" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
							</td>
						</tr>
					 </logic:if> 					 
					</table>
				 </logic:if>
				 <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BT')}">				        
			        <table>
					   <logic:if test="${not empty activityDetails.qtyDesc && activityDetails.qtyDesc != 'None'}">	
					        <logic:if test="${not empty activityDetails.linePattern && (activityDetails.linePattern ==3 || activityDetails.linePattern ==2)}">
					        <tr>
					        	<td>
							        <div class="col-xs-5 fs-8 text-black font-medium lh-base pe-4">
						          		<label for="ex2">${activityDetails.line1Desc} &nbsp; ${activityDetails.qtyOther}</label>
							        </div>					        	
					        	</td>
					        </tr>
					        </logic:if>
					        <%-- <logic:if test="${not empty activityDetails.linePattern && (activityDetails.linePattern ==3 || activityDetails.linePattern ==2 )}"> --%>
					         <logic:if test="${not empty activityDetails.linePattern && (activityDetails.linePattern ==3 && activityDetails.qtyDesc != 'Apartments') }">
					        <tr>
						        <td>
							        <div class="col-xs-3 fs-5 text-black font-medium lh-base pe-4">
						          	${activityDetails.line2Desc}
						          	<logic:if test="${not empty activityDetails.line2Desc}">
										<logic:choose>
											<logic:when test="${not empty activityDetails.tempBtQtyFlag && (activityDetails.tempBtQtyFlag == 'Y' || activityDetails.tempBtQtyFlag == 'Yes')}">
												<select name="tempBtQtyFlag" id='purpose' onchange="showCheckbox(this.value);">
													<option value="No">No</option>
													<option value="Yes" selected="selected">Yes</option>				
												</select>
											</logic:when>
											<logic:otherwise>
												<select name="tempBtQtyFlag" id='purpose' onchange="showCheckbox(this.value);">
													<option value="No">No</option>
													<option value="Yes">Yes</option>				
												</select>
											</logic:otherwise>
										</logic:choose>
									</logic:if>
					
						          	</div>
						        </td>
					        </tr>
					        <tr>
					        	<td>
									<div style="display:none;" id="business">
									    <input type="hidden" align="left" id="qtyCheckbox" placeholder="Check Box" onclick="return clearQty()"> ${activityDetails.line3Desc}<font color="red">*</font>
								    	<input type="text" maxlength="8" id="quantity" placeholder="${activityDetails.placeholderDesc}" name="tempBtQty" value="${activityDetails.tempBtQty}" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
									</div>    		
					        	</td>
					        </tr>
					        </logic:if>
					        <logic:if test="${empty activityDetails.linePattern || !(activityDetails.linePattern ==3 && activityDetails.qtyDesc != 'Apartments') }">
					        <input type="hidden" id="purpose" name="tempBtQtyFlag" value="${activityDetails.tempBtQtyFlag}">
					        </logic:if>
					    </logic:if>
					
						<logic:if test="${not empty activityDetails.linePattern && activityDetails.linePattern == 1}">
						   <tr>
					        	<td>
								    <input type="hidden" align="left" id="qtyCheckbox" placeholder="Check Box" onclick="return clearQty()"> ${activityDetails.line3Desc}<font color="red">*</font>
							    	<input type="text" maxlength="8" id="quantity" placeholder="${activityDetails.placeholderDesc}" name="tempBtQty" value="${activityDetails.tempBtQty}" onkeypress="return event.charCode >= 48 && event.charCode <= 57">    		
					        	</td>
					        </tr>					        
						</logic:if>
			        </table>
				 </logic:if>   
				 
				 </div>
			                                </div>
			                            
			                            
                        </div>
                    </div>
                    
                    
                    <div class="container-fluid px-0 license-renewal-buttons pt-0 pb-3">
		                        <div class="lncv-park-inner pt-0">
		                            <div class="modify-content-btn pt-4 text-center">
		                                <button type="button" class="btn btn-primary license-renewal-back mt-2 text-uppercase px-4 me-3" onclick="back();">back</button>
		                                <button type="button" class="btn btn-primary license-renewal-next mt-2 text-uppercase px-4" onclick="validateForm()">NEXT</button>
		                                <a type="button" data-bs-toggle="modal" data-bs-target="#documents-submit-msg" class="btn btn-cancel license-renewal-cancel mt-2 px-0 ms-3 text-uppercase text-darkblue" onclick="cancel();">cancel</a>
		                            </div>
		                 <input type="hidden" name="renewalCode" value="${activityDetails.renewalCode}">
						<input type="hidden" name="permitNumber" value="${activityDetails.permitNumber}">						
						<input type="hidden" name="businessAccNo" value="${activityDetails.businessAccNo}">
						<input type="hidden" name="tempId" value="${activityDetails.tempId}">
				        <input type="hidden" name="actType" value="${activityDetails.actType}">
						<input type="hidden" name="qtyOther" value="${activityDetails.qtyOther}">
						<input type="hidden" name="screenName" value="<%=StringUtils.nullReplaceWithBlank(scrnNameForBack)%>">
						<input type="hidden" name="placeholderDesc" value="${activityDetails.placeholderDesc}">
						<input type="hidden" name="qtyDesc" value="${activityDetails.qtyDesc}">
		                            
		                        </div>
                           </div>

                </div>
            </div>
        </div>

 				

				</form:form>
 				
				
 
    </div>
</body>
</html>
