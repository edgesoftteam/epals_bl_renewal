<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="com.elms.model.*,com.elms.common.Constants,com.elms.util.StringUtils,java.util.List" %> 
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="logic"%>     
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>  
<%
String scrnNameForBack = Constants.UPLOAD_ATTACHMENT_DETAILS_SCREEN;
%>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">	
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
 <script src="js/jquery/3.5.1/jquery.min.js"></script>
 <script src="js/jquery/3.5.1/jquery-3.6.0.js"></script>
	
<style>
input[type='file'] {
  color: transparent;    / Hides your "No File Selected" /
  direction: rtl;        / Sets the Control to Right-To-Left /
}

.fileContainer {
    overflow: hidden;
    position: relative;
}

.fileContainer [type=file] {
    cursor: inherit;
    display: block;
    font-size: 999px;
    filter: alpha(opacity=0);
    min-height: 100%;
    min-width: 100%;
    opacity: 0;
    position: absolute;
    right: 0;
    text-align: right;
    top: 0;
}

/* Example stylistic flourishes */

.fileContainer {
    background: #2352A1;
    color: white;
    border-radius: .5em;
    float: left;
    padding: .5em;
}

.fileContainer [type=file] {
    cursor: pointer;
}
/* table, th, td {
  border: 1px solid black;
} */

/* button:disabled,
button[disabled]{
  border: 1px solid #999999;
  background-color: #cccccc;
  color: #666666;
}
 */ 

</style>
<script>
$(document).on('contextmenu', function () {
	return false;
});
</script>
<script type="text/javascript">
$(document).ready(function() {	
	window.setInterval("LogoutTimer(0)", 1000);
});

function back(){	
	 document.forms[0].action="${pageContext.request.contextPath}/redirectToBlOrBtForBack";
	 document.forms[0].submit();
}

function cancel(){
	 document.forms[0].action = "${pageContext.request.contextPath}/cancelSession";
     document.forms[0].submit();
}

function AddAttachment(this1){	
		var attachmentTypeId=this1.value;		
		document.getElementById("attachmentId").value = attachmentTypeId;		
		document.forms[0].action = "${pageContext.request.contextPath}/uploadAttachments";		
		document.forms[0].submit();		
}

function next(){	
		document.forms[0].action = "${pageContext.request.contextPath}/feeDetails";
		document.forms[0].submit();	
}
function resetData(){	
	document.forms[0].action="${pageContext.request.contextPath}/resetBlUploadAttachments";
    document.forms[0].submit();	
}

function enableAndDisableUpload(typeId,buttonId){	
	var file="fileInput"+typeId;
	if(document.getElementById(file).value==''){
		var id="attachmentTypeId("+typeId+")";
		document.getElementById(id).disabled = false;
	}	
  } 
  
function disableFileUpload(id){	
	$('input:file').filter(function(){		
        return this.files.length==0 
	}).prop('disabled', true);	
	
	var idVal="attachmentTypeId"+id;
	document.getElementById(idVal).disabled = false;	
	
	var idVal1='fileContainer'+id;	
	document.getElementById(idVal1).style.backgroundColor = "green";
} 


$( document ).ready(function() {
	
	var renewalCode = $('input[name=renewalCode]').val();	
		$.ajaxSetup({async: false});
		$.ajax('${pageContext.request.contextPath}/checkFileUploadedOrNot?renewalCode='+renewalCode, 
		{			
			dataType: 'json', // type of response data
			async: false,
			timeout: 500, // timeout milliseconds
			cache: false,
			success: function (data,status,xhr) {   // success callback function				
				var myJSON = JSON.stringify(data.uploadedTypeIds).replace("[","").replace("]","");								
					var uploadedIds = myJSON.split(",");
					
					if (uploadedIds[0]) {
						for(var i = 0; i < uploadedIds.length; i++) {					  	
						   $("#fileContainer"+uploadedIds[i]).hide();
						   $("#upld"+uploadedIds[i]).hide();
						   $("#attachmentTypeId"+uploadedIds[i]).hide();
						   $("#fileInput"+uploadedIds[i]).hide();	  
						}
					}
								
			},
			 /* error: function (jqXhr, textStatus, errorMessage) { // error callback 				
				alert('Error: ' + errorMessage);
			}  */
		});
		
		$.ajaxSetup({async: true});
	 
});  
  
</script>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    
    <script src="js/font-awesome-script.js"></script>
	    
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css"  href="css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="css/renewal.css" /> 
    <link rel="stylesheet" type="text/css" href="css/main-ext.css" />     
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css"> 
	
	<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css"/>
	<script type="text/javascript" src="fancybox/jquery.fancybox.js"></script>    	
    
    <script src="js/renewal.js"></script>  
    <script src="js/font-awesome-script.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.bundle.min.js"></script>
	<script src="vendor/bootstrap/js/logout-timer.js"></script>
	<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css"/>
	<script type="text/javascript" src="fancybox/jquery.fancybox.js"></script>    
	<script src="js/bootstrap/bootstrap-4.0.0.min.js"></script>	    
	
	
    <title>City of Burbank Business License  / Business Tax Renewal - Quantity</title>
</head> 
<body class="business-renewal-portal-page" style="zoom: 75%;" onload="enableQtyDetails()">

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-footer">
	<button type="button" class="btn btn-primary" data-dismiss="modal">I Understand</button>
</div>
</div>
</div>


    <div class="portal-page-header bg-light">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-4 col-sm-3 col-xl-2 pe-0">
                    <nav class="navbar navbar-light">
                        <div class="container p-0 m-0">
                            <a class="navbar-brand" href="#">
                                <img src="images/logo-color.png" alt="" width="214" height="auto">
                            </a>
                        </div>
                    </nav>
                </div>
              
             
            </div>
        </div>
    
    
				
<form:form name="attachmentDetailsForm" cssClass="login100-form validate-form p-b-3 p-t-5" modelAttribute="activityForm"  enctype="multipart/form-data" method="post" id="uploadAttachments">
	<input type="hidden" name="id" id="attachmentId"/>

    <div class="container-row-fluid">
        <div class="row align-items-start m-0">
            <div class="col-md-3 menu-col pt-5 px-3 vh-100 menu-contents menu-item">
                <div class="bg-white py-4 mb-1 px-2">
                    <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4">Bill No:<span class="fw-bold text-darkblue ps-2">${activityDetails.permitNumber}</span></p>
                </div>
                <div class="bg-white py-4 business-info mb-1">
                    <div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">Business Account No :</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 d-block fw-normal text-989898 text-uppercase">${activityDetails.businessAccNo}</span>
                    </div>
                    <div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">Business Type :</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 d-block fw-normal text-989898 text-capitalize">${activityDetails.description}</span>
                    </div>
                    <div class="pb-2 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">Business Email :</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 fw-normal d-block text-989898">
                    
                     <logic:choose>
				       	<logic:when test="${not empty activityDetails.tempEmail}"> ${activityDetails.tempEmail}</logic:when>
				       	<logic:otherwise> ${activityDetails.email}	</logic:otherwise>
				    </logic:choose>
				    
				    </span>
                    </div>
                </div>
                 
                <div class="bg-white py-4 business-info mb-1">
                    <div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">					      
                        			<strong>Business Address : </strong>
					      	</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 fw-normal text-989898 d-block text-uppercase">
                          ${activityDetails.strNo}&nbsp;${activityDetails.address}&nbsp;${activityDetails.unit}
                         <br>
                         ${activityDetails.businessCityStateZip}

                        </span>

                    </div>

                </div>
                
            <logic:if test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.mailStrNo) or (not empty activityDetails.mailStrName)}">
			    <div class="bg-white py-4 business-info mb-1">
                    <div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">					      
                        	<logic:if test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.mailStrNo) or (not empty activityDetails.mailStrName)}">
					      		<td width="30%"><strong>Business Mailing Address : </strong></td>
					      	</logic:if>
					      	</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 fw-normal text-989898 d-block text-uppercase">
                           <logic:if test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.mailStrNo) or (not empty activityDetails.mailStrName)}">
    					      	${activityDetails.mailStrNo} ${activityDetails.mailStrName}<br>
	    				      	${activityDetails.mailCity} ${activityDetails.mailState} ${activityDetails.mailZip}
    				       </logic:if>   

                        </span>  

                    </div>

                </div>
            </logic:if>
            </div>
            
            		<input type="hidden" name="renewalCode" value="${activityDetails.renewalCode}">
					<input type="hidden" name="permitNumber" value="${activityDetails.permitNumber}">
					<input type="hidden" name="businessAccNo" value="${activityDetails.businessAccNo}">
					<input type="hidden" name="tempId" value="${activityDetails.tempId}">
				    <input type="hidden" name="actType" value="${activityDetails.actType}">
				    <input type="hidden" name="totalFee" value="${activityDetails.totalFee}">
					<input type="hidden" name="screenName" value="<%=StringUtils.nullReplaceWithBlank(scrnNameForBack)%>">
					
					
            <div class="col-md-9 data-col bg-white content-item">
                <div class="modify-data-page-content modify-data" id="modify-content">
                    <div class="container-fluid px-0 edit-profile  sec-1">
                        <div class="edit-profile-header pb-3">
                            <h3 class="section-header">city of burbank community development department 

			                <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BL')}">
						        Business License
						    </logic:if>
						    <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BT')}">
						        Business Tax
						    </logic:if>
			     				Renewal Portal
                           </h3>
                        </div>
                        <div class="pt-3 license-renewal-content-wrap">
                            <div class="container-fluid px-0 edit-profile-form pt-2 sec-1">
                            </div>
                        </div>

					<div class="row m-0 p-0 align-items-center justify-content-between" style="max-width: 1100px;"  style="align:left" >
    					<div class="form-group row">
    					
    						<table style="margin-left:95px;">
							<tr>
								<td>&nbsp; </td>
								<td colspan="2">
						             <logic:if test=" ${not empty activityDetails.displayErrorMsg}">
										<div >
										  <div class="form-group">
										   <label for="exampleInputName2">
										 <font color="red"> ${activityDetails.displayErrorMsg}</font>     
										   </label>
											   </div>
												</div>
									</logic:if>
						              <logic:if test=" ${not empty activityDetails.displaySuccessMsg}">
										<div >
										   <div class="form-group">
										     <label for="exampleInputName2">
										       <font color="green"> ${activityDetails.displaySuccessMsg}</font>     
										     </label>
									       </div>
										</div>
									</logic:if>
							     </td>
							     <td>&nbsp; </td>
							</tr>
	
	<logic:forEach var="attachment" items="${attachmentList}">
		<logic:if test="${(attachment.downloadOrUploadType == 'U') or (attachment.downloadOrUploadType == 'B')}">		
	    <logic:if test="${empty attachment.documentURL}">
		         
	        	                 <div class="px-0 pt-4 pb-3 py-xl-4 row justify-content-between align-items-center m-0  border-top border-bottom">
                                    <p class="font-medium font-roboto fs-5 lh-base text-black text-start m-0 col-xl-6 col-12">${attachment.attachmentDesc}</p>
                                    <div class="d-inline-block col-xl-4 col-6 mt-4 mt-xl-0 d-flex align-items-center px-0">
                                       <logic:if test="${empty attachment.fileName}"> 
                                        <label id="fileContainer${attachment.attachmentTypeId}" class="bg-f8 cursor-pt border-radius-25 d-inline-block text-darkblue text-uppercase font-medium font-roboto fs-6 lh-base me-5 border-blue py-2 px-4">
                                        choose file
                                        
                                        <input id="fileInput${attachment.attachmentTypeId}" type="file" name="theFile[${attachment.attachmentId}]" onchange="disableFileUpload('${attachment.attachmentTypeId}');" value="${attachment.attachmentTypeId}" class="d-none form-control-file"/>
                                        </label>
                                        </logic:if>
                                        <button id="attachmentTypeId${attachment.attachmentTypeId}" type="submit" onclick="AddAttachment(this);" value="${attachment.attachmentTypeId}" class="rounded-0 fw-blod btn btn-primary upload-dmv-registration mt-0 mt-xl-2 mt-xxl-0 text-uppercase px-4 me-0 py-2 h-auto ms-xl-3">
                                        
                                       <span id="upld${attachment.attachmentTypeId}">
                                    
                                        upload
                                       
                                       </span>                                      
                                        </button>
                                    </div>
                                                                 <div class="d-inline-block col-xl-10 col-12 mt-4 mt-xl-0 d-flex align-items-center px-0">
                                 <p class="font-medium font-roboto fs-5 lh-base text-black text-start m-0 col-xl-12 col-20"><strong id="task"> ${attachment.fileName} </strong></p>
                             </div>
                                </div>
                           </logic:if>             
	 	                    <logic:if test="${not empty attachment.documentURL}">     
 	        	                 <div class="px-0 pt-4 pb-3 py-xl-4 row justify-content-between align-items-center m-0  border-top border-bottom">
                                    <p class="font-medium font-roboto fs-5 lh-base text-black text-start m-0 col-xl-6 col-12"> 
                                    ${attachment.attachmentDesc}
                                    </p>
                                    <div class="d-inline-block col-xl-4 col-6 mt-4 mt-xl-0 d-flex align-items-center px-0">
                                     <logic:if test="${empty attachment.fileName}">  
                                        <label id="fileContainer${attachment.attachmentTypeId}" class="bg-f8 cursor-pt border-radius-25 d-inline-block text-darkblue text-uppercase font-medium font-roboto fs-6 lh-base me-5 border-blue py-2 px-4">
                                          choose file
                                          <input id="fileInput${attachment.attachmentTypeId}" type="file" name="theFile[${attachment.attachmentId}]" onchange="disableFileUpload('${attachment.attachmentTypeId}');" value="${attachment.attachmentTypeId}"  class="d-none form-control-file"/>
                                        </label>
                                      </logic:if>  
                                        
                                        <button id="attachmentTypeId${attachment.attachmentTypeId}" type="submit" onclick="AddAttachment(this);" value="${attachment.attachmentTypeId}" class="rounded-0 fw-blod btn btn-primary upload-dmv-registration mt-0 mt-xl-2 mt-xxl-0 text-uppercase px-4 me-0 py-2 h-auto ms-xl-3">
                                        <span id="upld${attachment.attachmentTypeId}">
                                        upload
                                        </span>
                                       </button>
                                    </div>
                                    
                                    
                             <div class="d-inline-block col-xl-10 col-12 mt-4 mt-xl-0 d-flex align-items-center px-0">
                                 <p class="font-medium font-roboto fs-5 lh-base text-black text-start m-0 col-xl-12 col-20"><strong id="task"> ${attachment.fileName} </strong></p>
                             </div>
                                </div>   

                                
                          </logic:if>    
                                                   

                         
                           
                         </logic:if>
                             	
	                   </logic:forEach>
	                   </table>
                            </div>
    	         </div>
            </div>                                
         </div>
			                            
			                            
              
                    
                    <div class="container-fluid px-0 license-renewal-buttons pt-0 pb-3">
		                        <div class="lncv-park-inner pt-0">
		                            <div class="modify-content-btn pt-4 text-center">
		                                <button type="button" class="btn btn-primary license-renewal-back mt-2 text-uppercase px-4 me-3" onclick="back();">back</button>
		                                <button type="button" class="btn btn-primary license-renewal-next mt-2 text-uppercase px-4" onclick="next()">NEXT</button>
		                                <button type="button" class="btn btn-primary license-renewal-next mt-2 text-uppercase px-4" onclick="resetData();">RESET</button>
		                                
		                                <a type="button" data-bs-toggle="modal" data-bs-target="#documents-submit-msg" class="btn btn-cancel license-renewal-cancel mt-2 px-0 ms-3 text-uppercase text-darkblue" onclick="cancel();">cancel</a>
		                            </div>

		                            
		                        </div>
                           </div>

                </div>
            </div>
        </div>

 				

				</form:form>
 				
				
 
    </div>
    <script src="vendor/animsition/js/animsition.min.js"></script>
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="vendor/select2/select2.min.js"></script>
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<script src="vendor/countdowntime/countdowntime.js"></script>
</body>
</html>
