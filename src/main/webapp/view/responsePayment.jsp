<%@page import="com.elms.util.StringUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="com.elms.model.*,com.elms.common.Constants,com.elms.util.StringUtils,java.util.HashMap,java.util.Map" %> 
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="logic"%>  
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
Activity activity= (Activity)request.getSession().getAttribute("activity");
String renewalCode = "";
String paymentReceiptUrl = "";
String prodFlag = "";

System.out.println("responsePayment.jsp :: Entered");

if(activity!=null){
	renewalCode = activity.getRenewalCode();
	if(renewalCode==null) renewalCode="";
	System.out.println("responsePayment.jsp :: in jsp 17..renewalCode :: "+renewalCode);

}

Map<String, String> lkupSystemDataMap = new HashMap<String,String>();

lkupSystemDataMap = (Map<String,String>) request.getSession().getAttribute("lkupSystemDataMap");
System.out.println("lkupSystemDataMap :"+lkupSystemDataMap.size());

prodFlag = lkupSystemDataMap.get(Constants.PROD_SERVER_FLAG);
System.out.println("prodFlag :: " +prodFlag);

if(activity.getPermitNumber()!=null && activity.getPermitNumber().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BL)){
	paymentReceiptUrl = lkupSystemDataMap.get(Constants.BL_PAYMENT_RECEIPT_URL);
}

if(activity.getPermitNumber()!=null && activity.getPermitNumber().startsWith(Constants.ACTIVITY_NUMBER_STARTS_WITH_BT)){
	if(prodFlag.equalsIgnoreCase("Y")) {
		paymentReceiptUrl = lkupSystemDataMap.get(Constants.BT_PAYMENT_RECEIPT_PROD_URL);
		System.out.println("responsePayment.jsp :: paymentReceiptUrl :: If Condition " +paymentReceiptUrl);
	}else{
		paymentReceiptUrl = lkupSystemDataMap.get(Constants.BT_PAYMENT_RECEIPT_NON_PROD_URL);
		System.out.println("responsePayment.jsp :: paymentReceiptUrl :: else Condition " +paymentReceiptUrl);
	}	
}

System.out.println("blPaymentReceiptUrl before appending act_id : "+paymentReceiptUrl);

paymentReceiptUrl = paymentReceiptUrl.concat(renewalCode);
System.out.println("blPaymentReceiptUrl after appending act_id :"+paymentReceiptUrl);

%>

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">	
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script src="js/jquery-3.3.1.min.js"></script>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="stylesheet" type="text/css" href="css/es_styles.css"/>  
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css"  href="css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="css/renewal.css" /> 
    <link rel="stylesheet" type="text/css" href="css/main-ext.css" /> 
    <script src="js/jquery/3.5.1/jquery.min.js"></script>
    <script src="js/font-awesome-script.js"></script>
    <script src="js/renewal.js"></script>  
         
    <script type="text/javascript" src="js/bootstrap/bootstrap.bundle.min.js"></script>
	<script src="vendor/bootstrap/js/logout-timer.js"></script>
	<script src="js/popper/popper-1.12.9.min.js" ></script>
	<script src="js/bootstrap/4.0.0/js/bootstrap-4.0.0.min.js"></script>	    
    
    <script>
$(document).on('contextmenu', function () {
	return false;
});
</script>
<script src="js/jquery-1.12.4.min.js"></script>
<script type="text/javascript">

$(document).ready(function() {	
	window.setInterval("LogoutTimer(0)", 1000);
});

function back(){	
	 document.forms[0].action="${pageContext.request.contextPath}/backHistory";
	 document.forms[0].submit();
}

function cancel(){
	 document.forms[0].action = "${pageContext.request.contextPath}/cancelSession";
     document.forms[0].submit();
}

</script>

<title>Business License\Tax Renewal Application Submitted Successfully Page</title>
</head>
<body class="tax-renewal-application-submitted" style="zoom: 75%;" onload="enableBusiness()">


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-footer">
	<button type="button" class="btn btn-primary" data-dismiss="modal">I Understand</button>
</div>
</div>
</div>

    <div class="portal-page-header bg-light">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-4 col-sm-3 col-xl-2 pe-0">
                    <nav class="navbar navbar-light">
                        <div class="container p-0 m-0">
                            <a class="navbar-brand" href="#">
                                <img src="images/logo-color.png" alt="" width="214" height="auto">
                            </a>
                        </div>
                    </nav>
                </div>
                
            </div>
        </div>
    </div>
    <div class="container-row-fluid">
        <div class="row align-items-start m-0">
           
            <div class="col-md-9 data-col bg-white content-item">
                <div class="my-permit-data-content" id="my-permits-1">
                    <div class="edit-profile-header pb-3">
                        <h3 class="section-header">city of burbank community development department
                         <logic:if test = "${fn:startsWith(activity.permitNumber, 'BL')}">
			        Business License
			    </logic:if>
			    <logic:if test = "${fn:startsWith(activity.permitNumber, 'BT')}">
			        Business Tax
			    </logic:if>  Renewal Portal
			    </h3>
                    </div>
                    <div class="pt-2 container-fluid px-0 my-permit-data">

<form:form name="previewForm" cssClass="login100-form validate-form p-b-33 p-t-5" modelAttribute="activityForm" method="post" action="${pageContext.request.contextPath}/paymentPage" class="form-horizontal">
	                    
                    <%if(activity.getResponseCode()!=null && activity.getResponseCode().equals("1")){%>	
                        <div class="show" id="payment-confirmation" >
                            <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" style="max-width: 920px;">
                              <div class="modal-content py-5">
                                <div class="modal-header d-block text-center pb-4 border-none">
                                    <i class="text-green fas fa-check-circle"></i>
	                                  <h5 class="modal-title w-100 m-auto fs-2 font-medium pt-4 lh-base text-capitalize text-center text-green" id="paymentModalLabel">
	                                    <logic:if test = "${fn:startsWith(activity.permitNumber, 'BL')}">
									        Business License
									    </logic:if>
									    <logic:if test = "${fn:startsWith(activity.permitNumber, 'BT')}">
									        Business Tax
									    </logic:if>  Renewal Transaction Is Successful
	                                  </h5>
                                </div>
                                <div class="modal-border pt-4 pb-4 px-5">
                                    <span class="d-block pt-2 px-5"></span>
                                </div>
                                <div class="modal-body payee-info overflow-visible text-center pb-0">
                                    <div class="container-fluid px-0 w-50 m-auto">
                                        <div class="payee-name d-flex justify-content-between align-items-center pb-3">
                                            <span class="text-capitalize d-block col-8 fs-4 fw-normal text-start lh-base text-535353">Payee Name :</span>
                                            <span class="d-block col-4 fs-4 text-start fw-normal text-start lh-base">${activity.name}</span>
                                        </div>
                                        <div class="permit-number d-flex justify-content-between align-items-center pb-3">
                                            <span class="text-capitalize d-block col-8 fs-4 fw-normal text-start lh-base text-535353">Transaction ID :</span>
                                            <span class="d-block text-uppercase col-4 fs-4 text-start fw-normal text-start lh-base">${activity.onlineTransactionId}</span>
                                        </div>
                                        <div class="total-amount d-flex justify-content-between align-items-center">
                                            <span class="text-capitalize d-block col-8 fs-4 fw-normal text-start lh-base text-535353">Total Amount :</span>
                                            <span class="d-block col-4 fs-4 text-start fw-normal text-start lh-base">${activity.totalFee}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer justify-content-center align-items-center pt-5 mt-2 pb-5 border-none">
                                  <a href="<%=paymentReceiptUrl%>" class="me-3 px-5 btn btn-primary fw-bold fs-6 text-uppercase text-white bg-darkblue" style="height: 60px;" target="_blank">PRINT RECEIPT</a>
                                  <button type="submit" class="ms-2 btn btn-secondary fw-bold fs-6 text-uppercase  bg-white px-5"  onclick="cancel()">EXIT</button>
                                </div>
                              </div>
                            </div>
                        </div>
                        

                       <%}else{%>

                        <div class="show" id="payment-confirmation" >
                            <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" style="max-width: 920px;">
                              <div class="modal-content py-5">
                                <div class="modal-header d-block text-center pb-4 border-none">
                                    <i class="text-green fas fa-check-circle"></i>
                                  <h5 class="modal-title w-100 m-auto fs-2 font-medium pt-4 lh-base text-capitalize text-center text-red" style="color:red"  id="paymentModalLabel">
                                  <logic:if test = "${fn:startsWith(activity.permitNumber, 'BL')}">
							        Business License
							    </logic:if>
							    <logic:if test = "${fn:startsWith(activity.permitNumber, 'BT')}">
							        Business Tax
							    </logic:if>  Renewal Transaction Is Declined
							 <br><br>   <%=activity.getResponseText() %>
                                  </h5>
                                </div>
                                <div class="modal-footer justify-content-center align-items-center pt-5 mt-2 pb-5 border-none">
                                  
                                  <button type="submit" class="ms-2 btn btn-secondary fw-bold fs-6 text-uppercase  bg-white px-5"  onclick="cancel()">EXIT</button>
                                </div>
                                <div class="modal-border pt-4 pb-4 px-5">
                                    <span class="d-block pt-2 px-5"></span>
                                </div>
                                
                           <%} %>
                           
                           
</form:form>	
                              </div>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<script type="text/javascript" src="bootstrap-5.0.2-dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
