<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="com.elms.model.*,com.elms.common.Constants,com.elms.util.StringUtils,java.util.List,java.net.URLEncoder" %> 
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="logic"%>     
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>  
<%
String scrnNameForBack = Constants.DOWNLOAD_ATTACHMENT_DETAILS_SCREEN;
String filePath = "";
%>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
    <script src="js/jquery/3.5.1/jquery.min.js"></script>
    <script src="js/jquery/3.5.1/jquery-3.6.0.js"></script>
    
    <script src="js/font-awesome-script.js"></script>
 	
<style>
input[type='file'] {
  color: transparent;    / Hides your "No File Selected" /
  direction: rtl;        / Sets the Control to Right-To-Left /
}
</style>

<script>
$(document).on('contextmenu', function () {
	return false;
});
$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
</script>
<script type="text/javascript">
$(document).ready(function() {	
	window.setInterval("LogoutTimer(0)", 1000);
});

function back(){	
	 document.forms[0].action="${pageContext.request.contextPath}/redirectToBlOrBtForBack";
	 document.forms[0].submit();
}

function cancel(){
	 document.forms[0].action = "${pageContext.request.contextPath}/cancelSession";
     document.forms[0].submit();
}

function AddAttachment(this1){
		var attachmentTypeId=this1.value;
		document.forms[0].action = "${pageContext.request.contextPath}/uploadAttachments?id="+attachmentTypeId;
		document.forms[0].submit();
	}
	
function next(){
	document.forms[0].action="${pageContext.request.contextPath}/redirectToBlOrBt";
    document.forms[0].submit();	
}

function enableAndDisableUpload(typeId,buttonId){
	var file="fileInput("+typeId+")";
	if(document.getElementById(file).value==''){
		var id="attachmentTypeId("+typeId+")";
		document.getElementById(id).disabled = false;
	}	
}

$( document ).ready(function() {
	var comments = $('input[name=comments]').val().replace("[","").replace("]","");
	var bmcPopUpWindowUrl = $('input[name=bmcPopUpWindowUrl]').val();
	
	var bmcDescription = $('input[name=bmcDescription]').val().replace("{","").replace("}","");
	var bmclinkDesc = bmcDescription.split(",");
	var cmnts = '';	
	var count = 0;

	for(var i = 0; i < bmclinkDesc.length; i++) {	
		cmnts = cmnts+'&bmcDesc'+i+'='+bmclinkDesc[i].trim().replace("#","!!!");
		count++;		
	}
	
	cmnts = cmnts+"&count="+count;
	
	if(comments==''){
		//console.log('<a title="comments" id="listTemplate"  href="${pageContext.request.contextPath}/bmcDescription?comments='+cmnts+'" >Friendly description</a>');
			$('<a title="comments" id="listTemplate"  href="${pageContext.request.contextPath}/bmcDescription?comments='+cmnts+'" >Friendly description</a>').fancybox({
			type: 'iframe',
		    width: '60%',
		    height: '60%',	
		    autoScale: false,
		    transitionIn: 'none',
		    transitionOut: 'none',
		    fitToView: true,
		    autoSize: false	
		}).click();	
	}else{
		 $('<a title="comments" id="listTemplate"  href="${pageContext.request.contextPath}/bmcDescription?comments='+comments+cmnts+'" >Friendly description</a>').fancybox({
         'width': '60%',
         'height': '60%',
         'autoScale': false,
         'transitionIn': 'none',
         'transitionOut': 'none',
         'fitToView': false,
         'autoSize' : false,
         'type' : 'iframe'
      
    }).click();
 }
});

</script>

<style type="text/css">
.fancybox-outer {
 background-color: #f2f3f4; /* or whatever */
}

.fancybox-content {
 border-color: #F0F8FF !important; /* or whatever */
}

.fancybox-skin {
 background-color: #333399 !important; /* or whatever */ 
 padding: 5px !important; 
}

</style>
 
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	
	    
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css"  href="css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="css/renewal.css" /> 
    <link rel="stylesheet" type="text/css" href="css/main-ext.css" />     
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css"> 
	
	<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css"/>
	<script type="text/javascript" src="fancybox/jquery.fancybox.js"></script>    	
    
    <script src="js/renewal.js"></script>  
      
    <script type="text/javascript" src="js/bootstrap/bootstrap.bundle.min.js"></script>
	<script src="vendor/bootstrap/js/logout-timer.js"></script>
	<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css"/>
	<script type="text/javascript" src="fancybox/jquery.fancybox.js"></script>    
	<script src="js/bootstrap/bootstrap-4.0.0.min.js"></script>	    
	
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<script src="vendor/select2/select2.min.js"></script>
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<script src="vendor/countdowntime/countdowntime.js"></script>
	
    <title>City of Burbank Business License  / Business Tax Renewal - Quantity</title>
</head> 
<body class="business-renewal-portal-page" style="zoom: 75%;" onload="enableQtyDetails()">

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-footer">
	<button type="button" class="btn btn-primary" data-dismiss="modal">I Understand</button>
</div>
</div>
</div>


    <div class="portal-page-header bg-light">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-4 col-sm-3 col-xl-2 pe-0">
                    <nav class="navbar navbar-light">
                        <div class="container p-0 m-0">
                            <a class="navbar-brand" href="#">
                                <img src="images/logo-color.png" alt="" width="214" height="auto">
                            </a>
                        </div>
                    </nav>
                </div>
              
             
            </div>
        </div>
    
    
<form:form name="attachmentDetailsForm" cssClass="login100-form validate-form p-b-3 p-t-5" modelAttribute="activityForm"  enctype="multipart/form-data" method="post" >

	<input type="hidden" name="renewalCode" value="${activityDetails.renewalCode}" />
	<input type="hidden" name="permitNumber" value="${activityDetails.permitNumber}" />
	<input type="hidden" name="businessAccNo" value="${activityDetails.businessAccNo}" />
	<input type="hidden" name="tempId" value="${activityDetails.tempId}" />
    <input type="hidden" name="actType" value="${activityDetails.actType}" />
    <input type="hidden" name="totalFee" value="${activityDetails.totalFee}" />
	<input type="hidden" name="screenName" value="<%=StringUtils.nullReplaceWithBlank(scrnNameForBack)%>" />
 	

	
    <div class="container-row-fluid">
        <div class="row align-items-start m-0">
            <div class="col-md-3 menu-col pt-5 px-3 vh-100 menu-contents menu-item">
                <div class="bg-white py-4 mb-1 px-2">
                    <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4">Bill No:<span class="fw-bold text-darkblue ps-2">${activityDetails.permitNumber}</span></p>
                </div>
                <div class="bg-white py-4 business-info mb-1">
                    <div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">Business Account No :</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 d-block fw-normal text-989898 text-uppercase">${activityDetails.businessAccNo}</span>
                    </div>
                    <div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">Business Type :</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 d-block fw-normal text-989898 text-capitalize">${activityDetails.description}</span>
                    </div>
                    <div class="pb-2 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">Business Email :</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 fw-normal d-block text-989898">
                    
                     <logic:choose>
				       	<logic:when test="${not empty activityDetails.tempEmail}"> ${activityDetails.tempEmail}</logic:when>
				       	<logic:otherwise> ${activityDetails.email}	</logic:otherwise>
				    </logic:choose>
				    
				    </span>
                    </div>
                </div>
                 
                <div class="bg-white py-4 business-info mb-1">
                    <div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">					      
                        			<strong>Business Address : </strong>
					      	</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 fw-normal text-989898 d-block text-uppercase">
                          ${activityDetails.strNo}&nbsp;${activityDetails.address}&nbsp;${activityDetails.unit}
                         <br>
                         ${activityDetails.businessCityStateZip}

                         

                        </span>

                    </div>

                </div>
                
<logic:if test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.mailStrNo) or (not empty activityDetails.mailStrName)}">
					      	
                <div class="bg-white py-4 business-info mb-1">
                    <div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">					      
                        	<logic:if test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.mailStrNo) or (not empty activityDetails.mailStrName)}">
					      		<td width="30%"><strong>Business Mailing Address : </strong></td>
					      	</logic:if>
					      	</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 fw-normal text-989898 d-block text-uppercase">
                           <logic:if test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.mailStrNo) or (not empty activityDetails.mailStrName)}">
    					      	${activityDetails.mailStrNo} ${activityDetails.mailStrName}<br>
	    				      	${activityDetails.mailCity} ${activityDetails.mailState} ${activityDetails.mailZip}
    				       </logic:if>   

                        </span>  

                    </div>

                </div>
                </logic:if>
            </div>
            <div class="col-md-9 data-col bg-white content-item">
                <div class="modify-data-page-content modify-data" id="modify-content">
                    <div class="container-fluid px-0 edit-profile  sec-1">
                        <div class="edit-profile-header pb-3">
                            <h3 class="section-header">city of burbank community development department 

			                <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BL')}">
						        Business License
						    </logic:if>
						    <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BT')}">
						        Business Tax
						    </logic:if>
			     				Renewal Portal
                           </h3>
                        </div>
                        <div class="pt-3 license-renewal-content-wrap">
                            <div class="container-fluid px-0 edit-profile-form pt-2 sec-1">
                            </div>
                        </div>

<div class="row m-0 p-0 align-items-center justify-content-between" style="max-width: 1100px;"  style="align:left" >
    <div class="form-group row">
		         
	        	                <div class="modal-content px-0">
                                    <div class="modal-header d-block text-center pt-5 px-3 pb-3 border-none">
                                       <h5 class="modal-title w-100 m-auto fs-2 font-medium pt-4 lh-base text-capitalize text-center text-green" id="Supplemental-Application">Supplemental Application</h5>
                                    </div>
                                    <div class="modal-border pt-2 pb-3 px-5">
                                        <span class="d-block px-5"></span>
                                    </div>
                                    
                                    <div class="pb-5 px-3 pt-4 text-center">
                                    
                                   	<table style="margin-left:80px;">
									<tr>
										<td>&nbsp; </td>
								
										<td colspan="2">
								             <logic:if test=" ${not empty activityDetails.displayErrorMsg}">
												<div >
												  <div class="form-group">
												   <label for="exampleInputName2">
												 <font color="red"> ${activityDetails.displayErrorMsg}</font>     
												   </label>
													   </div>
														</div>
											</logic:if>
								              <logic:if test=" ${not empty activityDetails.displaySuccessMsg}">
												<div >
												   <div class="form-group">
												     <label for="exampleInputName2">
												       <font color="green"> ${activityDetails.displaySuccessMsg}</font>     
												     </label>
											       </div>
												</div>
											</logic:if>
									     </td>
									     <td>&nbsp; </td>
									</tr>
                                
                                                                    
                              <logic:forEach var="attachment" items="${attachmentList}">	
		                      <logic:if test="${(attachment.downloadOrUploadType == 'D') or (attachment.downloadOrUploadType == 'B')}">	
		 	                  <logic:if test="${not empty attachment.documentURL}"> 
                                    <div class="p-3 d-inline-block">
                       					<logic:if test="${attachment.documentURL == 'No documents to download'}"><font color="red"><strong>${attachment.attachmentDesc}</strong></font></logic:if>                          
               	   	   					<logic:if test="${attachment.documentURL != 'No documents to download'}"><strong>${attachment.attachmentDesc}</strong></logic:if>   
                                     </div> 
                                      
                                      <div class="p-3 d-inline-block">
                       						<logic:if test="${attachment.documentURL != 'No documents to download'}">
											<logic:set var="fileLoc" value="${attachment.documentURL}" scope="page" />
											<%
												filePath = String.valueOf(pageContext.getAttribute("fileLoc"));
												filePath = URLEncoder.encode(filePath);
											%>
											
												<!-- <a href="${pageContext.request.contextPath}/downloadAttachment?fileName=${attachment.attachmentDesc}&filePath=${attachment.documentURL}"> -->
												<a href="${pageContext.request.contextPath}/downloadAttachment?fileName=${attachment.attachmentDesc}&filePath=<%=filePath%>">
													<img src="images/PDF_icon.svg" width="40" height="auto" title="Download ${attachment.attachmentDesc}"/>
						                      	</a>&nbsp;Download
											</logic:if>
                         
                                        </div>
                        <input type="hidden" name="comments" value="${attachment.commentsList}">						
						<input type="hidden" name="bmcPopUpWindowUrl" value="${attachment.bmcPopUpWindowUrl}">
						<input type="hidden" name="bmcDescription" value="${attachment.bmcLinkDescMap}">
                                        </logic:if>
			                         </logic:if>
			                         <br/>
                        		</logic:forEach>    
                        		</table>
                                    </div> 

                                </div>
                            </div>
    	        	    
				 
				 </div>
				 
	
                                
</div>                                
		    </div>
			                            
			                            
              
                    
                    <div class="container-fluid px-0 license-renewal-buttons pt-0 pb-3">
		                        <div class="lncv-park-inner pt-0">
		                            <div class="modify-content-btn pt-4 text-center">
		                                <button type="button" class="btn btn-primary license-renewal-back mt-2 text-uppercase px-4 me-3" onclick="back();">back</button>
		                                <button type="button" class="btn btn-primary license-renewal-next mt-2 text-uppercase px-4" onclick="next()">NEXT</button>
		                                <a type="button" data-bs-toggle="modal" data-bs-target="#documents-submit-msg" class="btn btn-cancel license-renewal-cancel mt-2 px-0 ms-3 text-uppercase text-darkblue" onclick="cancel();">cancel</a>
		                            </div>

		                            
		                        </div>
                           </div>

                </div>
            </div>
        </div>

 				

				</form:form>
 				
				
 
    </div>
    <script src="vendor/animsition/js/animsition.min.js"></script>
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="vendor/select2/select2.min.js"></script>
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<script src="vendor/countdowntime/countdowntime.js"></script>
</body>

</html>
