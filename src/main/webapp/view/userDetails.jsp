<!DOCTYPE html>
<%@page import="com.elms.util.StringUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="com.elms.model.*,com.elms.common.Constants,com.elms.util.StringUtils" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="logic"%>      
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%
String scrnNameForBack = Constants.USER_DETAILS_SCREEN;
  
%>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">	
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/es_styles.css"/>  
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css"  href="css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="css/renewal.css" /> 
    <link rel="stylesheet" type="text/css" href="css/main-ext.css" /> 
    <script src="js/jquery/3.5.1/jquery.min.js"></script>
    <script src="js/font-awesome-script.js"></script>
    <script src="js/renewal.js"></script>  
         
    <script type="text/javascript" src="js/bootstrap/bootstrap.bundle.min.js"></script>
	<script src="vendor/bootstrap/js/logout-timer.js"></script>
	<script src="js/popper/popper-1.12.9.min.js" ></script>
	<script src="js/bootstrap/4.0.0/js/bootstrap-4.0.0.min.js"></script>	    
    
    <script>
	$(document).on('contextmenu', function () {
	
		return false;
	});
   
	$(document).ready(function() {	
		window.setInterval("LogoutTimer(0)", 1000);
	});

function validateEmailAddress(email) {
	var email = document.forms[0].email.value;
	var flag = true;
	var splitted = email.match("^(.+)@(.+)$");
	if (splitted == null)
		flag = false;
	if (flag == true && splitted[1] != null) {
		var regexp_user = /^\"?[\w-_\.]*\"?$/;
		if (splitted[1].match(regexp_user) == null)
			flag = false;
	}
	if (flag == true && splitted[2] != null) {
		var regexp_domain = /^[\w-\.]*\.[A-Za-z]{2,4}$/;
		if (splitted[2].match(regexp_domain) == null) {
			var regexp_ip = /^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
			if (splitted[2].match(regexp_ip) == null)
				flag = false;
		}// if
	}
	if (email != '' && flag == false) {
		document.forms[0].elements['email'].value = '';
		alert("Invalid email address.");
		document.forms[0].elements['email'].focus();
  	  return false;
	}
}

function DisplayPhoneHyphen(str,evt){
	var str1;
	evt = (evt) ? evt : window.event;	
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (( charCode<48 || charCode>57 ) && (charCode != 46)){
		return false;
	}else{
		if ((document.forms[0].elements['newPhone'].value.length == 3 ) || (document.forms[0].elements['newPhone'].value.length == 7 )){
			
			document.forms[0].elements['newPhone'].value = document.forms[0].elements['newPhone'].value+'-';
			str1 = document.forms[0].elements[str].value;			
 		}
		if (document.forms[0].elements[str].value.length > 11 ) {
			return  false;
		}
	}
}

function ZipCodeValidation(str,evt){
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (( charCode<48 || charCode>57 ) && (charCode != 46)){
		return false;
	}else{
		if (document.forms[0].elements[str].value.length > 4 )  return false;
	}
}

function numberOnly(str, evt) {
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if ( charCode<48 || charCode>57 ){
		return false;
	}	
}

function validateForm(){
	if(document.forms[0].email.value=="" || document.forms[0].email.value==" "){
      alert("Please enter Business email");
      document.forms[0].email.focus();
   	  return false;
   	}else if(document.forms[0].strNo.value!="" || document.forms[0].address.value!="" || document.forms[0].city.value!="" || document.forms[0].state.value!="" || document.forms[0].zip.value!=""){
   		var strName=document.forms[0].address.value;
   		var str=false;

   		if(strName!=null && (strName.substring(0,2)=='PO' || strName.substring(0,3) =='P.O' || strName.substring(0,3) =='P O')){
   			str=true;
   		}
   		if(document.forms[0].strNo.value=="" && (str==false)){
    	  alert("Please enter mailing street number");
    	  document.forms[0].strNo.focus(); 
    	  return false;
    	}else if(document.forms[0].address.value==""){
    	  alert("Please enter mailing street Name");
    	  document.forms[0].address.focus();
    	  return false;
    	}else if(document.forms[0].city.value==""){
	      alert("Please enter mailing city");
	      document.forms[0].city.focus();
	   	  return false;
    	}else if(document.forms[0].state.value==""){
	      alert("Please enter mailing state");
	      document.forms[0].state.focus();
    	  return false;
    	}else if(document.forms[0].zip.value==""){
	      alert("Please enter mailing zip");
	      document.forms[0].zip.focus();
    	  return false;
    	}else if(isNaN(document.forms[0].zip.value)){
	      alert("Please enter valid zip code");
	      document.forms[0].zip.focus();	      
    	  return false;
    	}else{  
        	document.forms[0].action="${pageContext.request.contextPath}/redirectToBlOrBt";
	   		document.forms[0].submit();
	   		return true;
    	}
  	}

	document.forms[0].action="${pageContext.request.contextPath}/redirectToBlOrBt";	
    document.forms[0].submit();
    return true;
 }
	
	function cancel(){
		 document.forms[0].action = "${pageContext.request.contextPath}/cancelSession";
	     document.forms[0].submit();
	     return true;
	}
	
	function IsAlphaNumeric(e) {
			var k;
		    document.all ? k = e.keyCode : k = e.which;
		    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
	 }
	</script>

	    
    
    
    <title>City of Burbank Business License  / Business Tax Renewal - Renewal User Details</title>
</head>
<body class="business-renewal-portal-page" style="zoom: 75%;">
    <div class="portal-page-header bg-light">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-4 col-sm-3 col-xl-2 pe-0">
                    <nav class="navbar navbar-light">
                        <div class="container p-0 m-0">
                            <a class="navbar-brand" href="#">
                                <img src="images/logo-color.png" alt="" width="214" height="auto">
                            </a>
                        </div>
                    </nav>
                </div>
              
             
            </div>
        </div>
    
    
  <form:form name="userDetailsForm" cssClass="login100-form validate-form p-b-33 p-t-5" modelAttribute="activityForm" method="post" class="form-horizontal">
    
    <div class="container-row-fluid">
        <div class="row align-items-start m-0">
            <div class="col-md-3 menu-col pt-5 px-3 vh-100 menu-contents menu-item">
                <div class="bg-white py-4 mb-1 px-2">
                    <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4">Bill No:<span class="fw-bold text-darkblue ps-2">${activityDetails.permitNumber}</span></p>
                </div>
                <div class="bg-white py-4 business-info mb-1">
                	 <div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">Business Name :</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 d-block fw-normal text-989898 text-uppercase">${activityDetails.businessName}</span>
                    </div>
                    <div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">Business Account No :</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 d-block fw-normal text-989898 text-uppercase">${activityDetails.businessAccNo}</span>
                    </div>
                    <div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">Business Type :</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 d-block fw-normal text-989898 text-capitalize">${activityDetails.description}</span>
                    </div>
                    <div class="pb-2 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">Business Email :</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 fw-normal d-block text-989898">
                    
                     <logic:choose>
				       	<logic:when test="${not empty activityDetails.tempEmail}"> ${activityDetails.tempEmail}</logic:when>
				       	<logic:otherwise> ${activityDetails.email}	</logic:otherwise>
				    </logic:choose>
				    
				    </span>
                    </div>
                </div>
                 
                <div class="bg-white py-4 business-info mb-1">
                    <div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">					      
                        			<strong>Business Address : </strong></td>
					      	</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 fw-normal text-989898 d-block text-uppercase">
                          <%-- ${activityDetails.strNo}&nbsp;${activityDetails.address}&nbsp;${activityDetails.unit} --%>
                          ${activityDetails.businessStrName}
                         <br>
                         ${activityDetails.businessCityStateZip}

                         

                        </span>

                    </div>

                </div>
                
<logic:if test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.mailStrNo) or (not empty activityDetails.mailStrName)}">
					      	
                <div class="bg-white py-4 business-info mb-1">
                    <div class="pb-3 mb-0 px-2">
                        <p class="font-roboto font-medium text-black fs-18 lh-sm text-start m-0 px-4 pb-1">					      
                        	<logic:if test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.mailStrNo) or (not empty activityDetails.mailStrName)}">
					      		<td width="30%"><strong>Business Mailing Address : </strong></td>
					      	</logic:if>
					      	</p>
                        <span class="font-roboto text-black fs-18 lh-sm text-start m-0 px-4 fw-normal text-989898 d-block text-uppercase">
                           <logic:if test="${(not empty activityDetails.strNo) or (not empty activityDetails.address) or (not empty activityDetails.mailStrNo) or (not empty activityDetails.mailStrName)}">
    					      	<%-- ${activityDetails.mailStrNo} ${activityDetails.mailStrName}<br>
	    				      	${activityDetails.mailCity} ${activityDetails.mailState} ${activityDetails.mailZip} --%>
	    				      	
	    				      	
	    				      	<logic:choose>
								     	<logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.strName)}">${activityDetails.strNo} ${activityDetails.address} ${activityDetails.unit}<br>
	    				      	${activityDetails.city} ${activityDetails.state} ${activityDetails.zip}</logic:when>
									 	<logic:otherwise>${activityDetails.mailStrNo} ${activityDetails.mailStrName}<br>
	    				      	${activityDetails.mailCity} ${activityDetails.mailState} ${activityDetails.mailZip}</logic:otherwise>
							    </logic:choose>
	    				      	
    				       </logic:if>   

                        </span>

                    </div>

                </div>
                </logic:if>
            </div>
            <div class="col-md-9 data-col bg-white content-item">
                <div class="modify-data-page-content modify-data" id="modify-content">
                    <div class="container-fluid px-0 edit-profile  sec-1">
                        <div class="edit-profile-header pb-3">
                            <h3 class="section-header">city of burbank community development department 

			                <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BL')}">
						        Business License
						    </logic:if>
						    <logic:if test = "${fn:startsWith(activityDetails.permitNumber, 'BT')}">
						        Business Tax
						    </logic:if>
			     				Renewal Portal
                           </h3>
                        </div>
                         
                        <div class="pt-3 license-renewal-content-wrap">
                            <div class="container-fluid px-0 edit-profile-form pt-2 sec-1">
                                
                                    <div class="form-inner-wrap px-2 py-2">
                                    
                                    
                                    
                                        <div class="row align-items-start justify-content-between">
                                            <div class="col-12 col-lg-6 mb-3 form-field">
                                                <label for="business-Email" class="fs-18 lh-sm text-start fw-bold form-label mb-2 text-capitalize">Email*</label>
                                                <input type="email" class="fs-18 text-start form-control text-left bg-white"  name="email" 
													<logic:choose>
													  <logic:when test="${not empty activityDetails.tempEmail}">value="${activityDetails.tempEmail}" </logic:when>
													 	<logic:otherwise>value="${ activityDetails.email}"</logic:otherwise>
													</logic:choose> 
												aria-describedby="emailHelp" placeholder="Email" onblur="validateEmailAddress(this)">
                                            </div>
                                            <div class="col-12 col-lg-6 mb-3 form-field">
                                                <label for="business-phno" class="fs-18 lh-sm text-start fw-bold form-label mb-2 text-capitalize">Business Phone Number</label>
                                                <input type="text" class="fs-18 text-start form-control text-left bg-white"  id="newPhone" name="newPhone" 
												<logic:choose>
												       	<logic:when test="${not empty activityDetails.tempBusinessPhone}">value="${activityDetails.tempBusinessPhone}" </logic:when>
												 	<logic:otherwise>value="${activityDetails.businessPhone}"</logic:otherwise>
												 </logic:choose> class="form-control" placeholder="Phone Number"  pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" onkeypress="return DisplayPhoneHyphen('newPhone',event)">
                                            </div>
                                            <div class="col-12 col-lg-6 mb-3 form-field">
                                                <label for="business-streetno" class="fs-18 lh-sm text-start fw-bold form-label mb-2 text-capitalize">Mailing Street Number</label>
                                                <input type="text" class="fs-18 text-start form-control text-left bg-white" name="strNo" placeholder="Street Number" 
												<logic:choose>
												       	<logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.address)}">value="${activityDetails.strNo}" </logic:when>
												 	<logic:otherwise>value="${activityDetails.mailStrNo}"</logic:otherwise>
												 </logic:choose> class="form-control"  maxlength="6" onkeypress="return event.charCode >= 47 && event.charCode <= 57">
                                            </div>
                                            <div class="col-12 col-lg-6 mb-3 form-field">
                                                <label for="business-streetname" class="fs-18 lh-sm text-start fw-bold form-label mb-2 text-capitalize">Mailing Street Name</label>
                                                <input type="text" class="fs-18 text-start form-control text-left bg-white" name="address" placeholder="Street Name" 
												<logic:choose>
												       	<logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.strName)}">value="${activityDetails.strName}" </logic:when>
												 	<logic:otherwise>value="${activityDetails.mailStrName}"</logic:otherwise>
												 </logic:choose> maxlength="25" class="form-control">
                                            </div>
                                            <div class="col-12 col-lg-6 mb-3 form-field">
                                                <label for="business-unitno" class="fs-18 lh-sm text-start fw-bold form-label mb-2 text-capitalize">Mailing Unit/Suite Number</label>
                                                <input type="text" class="fs-18 text-start form-control text-left bg-white" name="unit"  placeholder="Unit" 
                                                <logic:choose>
											      <logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.address)}">value="${activityDetails.unit}" </logic:when>
											 	  <logic:otherwise>value="${activityDetails.mailUnit}"</logic:otherwise>
											    </logic:choose>  class="form-control" maxlength="5" onkeypress="return IsAlphaNumeric(event);">
                                            </div>
                                            <div class="col-12 col-lg-6 mb-3 form-field row justify-content-start ps-0">
                                                <div class="col-12 col-lg-6 ps-0">
                                                    <label for="business-city" class="fs-18 lh-sm text-start fw-bold form-label mb-2 text-capitalize">Mailing City</label>
                                                    <input type="text" class="fs-18 text-start form-control text-left bg-white" name="city"  placeholder="City" 
                                                    <logic:choose>
													   	<logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.address)}">value="${activityDetails.city}" </logic:when>
													 	<logic:otherwise>value="${activityDetails.mailCity}"</logic:otherwise>
													 </logic:choose>  maxlength="25" class="form-control">	
                                                </div>
                                                <div class="col-12 col-lg-6">
                                                    <label for="business-state" class="fs-18 lh-sm text-start fw-bold form-label mb-2 text-capitalize">Mailing State</label>
                                                    <input type="text" class="fs-18 text-start form-control text-left bg-white" name="state" placeholder="State" 
													<logic:choose>
													       	<logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.address)}">value="${activityDetails.state}" </logic:when>
													 	<logic:otherwise>value="${activityDetails.mailState}"</logic:otherwise>
													 </logic:choose>  maxlength="2" class="form-control">	
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-12 mb-0 form-field">
                                                <label for="business-zip" class="fs-18 lh-sm text-start fw-bold form-label mb-2 text-capitalize">Mailing Zip</label>
                                                <input type="text" class="fs-18 text-start form-control text-left bg-white"  name="zip" placeholder="Zip"
                                                 <logic:choose>
											       	<logic:when test="${(not empty activityDetails.strNo) or (not empty activityDetails.address)}">value="${activityDetails.zip}" </logic:when>
											 	    <logic:otherwise>value="${activityDetails.mailZip}"</logic:otherwise>
											     </logic:choose>  maxlength="5" class="form-control"  onkeypress="return ZipCodeValidation('zip', event)">
                                            </div>
                                        </div>
                                        <div class="container-fluid py-2 mt-4 mb-0 w-75 m-auto bg-white note-info position-relative" id="inspection-info">
                                            <a class="position-absolute d-inline-block top-0 end-0" style="background-color:#c3c3c3;"><i class="px-2 py-1 text-white fas fa-times"></i></a>
                                            <p class="text-red fs-6 lh-base fw-bold m-auto w-100 p-2 py-3 font-roboto text-center">Business email is required field.</p>
                                        </div>
                                    </div>
                                
                            </div>
                        </div>
                    </div>
                    
                    <div class="px-4 py-3 mt-3 indication-msg bg-darkblue  w-75 m-auto">
                        <p class="text-white text-center font-roboto font-medium fs-5 lh-base m-0">Please indicate any changes to your account below. (Changes to business location, business name or ownership require a new application. <span class="fw-bold">Please email us with your account number: <a class="text-white text-decoration-none" href="email:license@burbank.gov">license@burbankca.gov</a>).</span></p>
                    </div>
                    
                    <div class="container-fluid px-0 license-renewal-buttons pt-0 pb-3">
		                        <div class="lncv-park-inner pt-0">
		                            <div class="modify-content-btn pt-4 text-center">
		                                <button type="submit" class="btn btn-primary license-renewal-next mt-2 text-uppercase px-4" onclick="return validateForm();">NEXT</button>
		                                <a type="submit" data-bs-toggle="modal" data-bs-target="#documents-submit-msg" class="btn btn-cancel license-renewal-cancel mt-2 px-0 ms-3 text-uppercase text-darkblue" onclick="return cancel();">cancel</a>
		                            </div>
		                        </div>
                           </div>

                </div>
            </div>
        </div>
    </div>
    
	<input type="hidden" name="renewalCode" value=" ${activityDetails.renewalCode}">
	<input type="hidden" name="permitNumber" value=" ${activityDetails.permitNumber}">
	<input type="hidden" name="businessAccNo" value=" ${activityDetails.businessAccNo}">
	<input type="hidden" name="tempId" value=" ${activityDetails.tempId}">
	<input type="hidden" name="businessName" value=" ${activityDetails.businessName}">
	<input type="hidden" name="screenName" value="<%=StringUtils.nullReplaceWithBlank(scrnNameForBack)%>">
  </form:form>

</body>
</html>
